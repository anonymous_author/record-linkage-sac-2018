//
// Created by  on 18.04.18.
//
#include "preprocessor.hpp"

#include <regex>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Basic/TargetInfo.h>

namespace atf {
namespace cf {

void preprocess(const std::string &source, std::string &dest, const std::string &flags) {
    std::vector<std::string> defines;
    auto rgx                = std::regex("-D ([^ ]+=[^ ]+)");
    auto rgx_iterator_begin = std::sregex_iterator(flags.begin(), flags.end(), rgx);
    auto rgx_iterator_end   = std::sregex_iterator();
    while (rgx_iterator_begin != rgx_iterator_end) {
        defines.push_back(rgx_iterator_begin->str(1));
        rgx_iterator_begin++;
    }
    preprocess(source, dest, defines);
}

void preprocess(const std::string &source, std::string &dest, const std::vector<std::string> &defines) {
    clang::CompilerInstance ci;
    ci.createDiagnostics(); // create DiagnosticsEngine
    ci.createFileManager();  // create FileManager

    std::shared_ptr<clang::TargetOptions> options = std::make_shared<clang::TargetOptions>(ci.getTargetOpts());
    options->Triple = "nvptx";
    clang::TargetInfo *target = clang::TargetInfo::CreateTargetInfo(ci.getDiagnostics(), options);
    ci.setTarget(target);

    ci.createSourceManager(ci.getFileManager()); // create SourceManager

    // create buffer from source code string
    std::unique_ptr<llvm::MemoryBuffer> buffer = llvm::MemoryBuffer::getMemBufferCopy(source);
    clang::FrontendInputFile buffer_file(buffer.get(), clang::IK_OpenCL);
    ci.InitializeSourceManager(buffer_file);

    // add macro definitions
    clang::PreprocessorOptions &opts = ci.getPreprocessorOpts();
    for (const auto &def : defines) {
        opts.addMacroDef(def);
    }

    // create output stream
    llvm::raw_string_ostream out(dest);

    // preprocess
    ci.createPreprocessor(clang::TU_Complete);  // create Preprocessor
    ci.getPreprocessorOutputOpts().ShowCPP = 1;
    DoPrintPreprocessedInput(ci.getPreprocessor(), &out, ci.getPreprocessorOutputOpts());
    buffer.release();
    // flush output stream
    out.str();
}

}
}