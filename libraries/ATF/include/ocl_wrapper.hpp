//
// Created by  on 13.05.2017.
//

#ifndef OCL_WRAPPER_HPP
#define OCL_WRAPPER_HPP

#define SAVE_BINARY 0

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>

#include <cstddef>
#include <tuple>
#include <chrono>
#include <unistd.h> // for sleep

#include "helper.hpp"
#include "process_wrapper_helper.hpp"

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include "tp_value.hpp"

#undef  __CL_ENABLE_EXCEPTIONS

#include <fstream>
#include <thread>
#include <future>
#include <netdb.h>
#include <iomanip>
#include <iostream>

//#define USE_CLANG_PREPROCESSOR
#ifdef USE_CLANG_PREPROCESSOR
#include "preprocessor.hpp"
#endif

//enables lazy evaluation
namespace
{
template< typename T >
class wrapper
{
    // save l-values as references and r-values by value
    using T_ref_free  = typename std::remove_reference<T>::type;
    using T_save_type = typename std::conditional_t< std::is_rvalue_reference<T>::value, T_ref_free, T_ref_free& >;

public:
    wrapper( T value )
            : _value( value )
    {}


    T_ref_free get_value()
    {
        return _value;
    }


private:
    T_save_type _value;
};
}

namespace atf {
namespace cf {

using nd_range_t              = std::array<size_t,3>;
using thread_configurations_t = std::map< ::atf::configuration, std::array<nd_range_t,2> >;


void check_error(cl_int err);

void check_fatal_error(cl_int err);

#if 0

template< typename T_lhs_0, typename T_rhs_0, typename callable_0,
          typename T_lhs_1, typename T_rhs_1, typename callable_1,
          typename T_lhs_2, typename T_rhs_2, typename callable_2
        >
auto GS( op_wrapper_class< T_lhs_0, T_rhs_0, callable_0 > gs_0,
         op_wrapper_class< T_lhs_1, T_rhs_1, callable_1 > gs_1,
         op_wrapper_class< T_lhs_2, T_rhs_2, callable_2 > gs_2
       )
{
  //return std::array< int, 3 >( gs_0.cast(), gs_1.cast(), gs_2.cast() );
  return std::make_tuple( gs_0, gs_1, gs_2 );
}


template< typename T_lhs_0, typename T_rhs_0, typename callable_0,
          typename T_lhs_1, typename T_rhs_1, typename callable_1,
          typename T_lhs_2, typename T_rhs_2, typename callable_2
        >
auto LS( op_wrapper_class< T_lhs_0, T_rhs_0, callable_0 > ls_0,
         op_wrapper_class< T_lhs_1, T_rhs_1, callable_1 > ls_1,
         op_wrapper_class< T_lhs_2, T_rhs_2, callable_2 > ls_2
       )
{
  //return std::array< int, 3 >( ls_0.cast(), ls_1.cast(), ls_2.cast() );
  return std::make_tuple( ls_0, ls_1, ls_2 );
}

#endif

#if 0


template< typename GS_0, typename GS_1, typename GS_2 >
auto GS( GS_0 gs_0, GS_1 gs_1, GS_2 gs_2 )
{
  auto gs_0_val = typename std::conditional<std::is_fundamental<GS_0>::value, scalar_wrapper<GS_0>, GS_0>::type(gs_0);
  auto gs_1_val = typename std::conditional<std::is_fundamental<GS_1>::value, scalar_wrapper<GS_1>, GS_1>::type(gs_1);
  auto gs_2_val = typename std::conditional<std::is_fundamental<GS_2>::value, scalar_wrapper<GS_2>, GS_2>::type(gs_2);

  return std::make_tuple( gs_0_val, gs_1_val, gs_2_val );
}


template< typename LS_0, typename LS_1, typename LS_2 >
auto LS( LS_0 ls_0, LS_1 ls_1, LS_2 ls_2 )
{
  return std::make_tuple( ls_0, ls_1, ls_2 );
}

#endif


#if 1

template< typename T_0, typename T_1 = size_t, typename T_2 = size_t >
auto GS( T_0&& gs_0, T_1&& gs_1 = 1, T_2&& gs_2 = 1 )
{
    return std::make_tuple( wrapper<T_0&&>( std::forward<T_0>(gs_0) ), wrapper<T_1&&>( std::forward<T_1>(gs_1) ), wrapper<T_2&&>( std::forward<T_2>(gs_2) ) );
}


template< typename T_0, typename T_1 = size_t, typename T_2 = size_t >
auto LS( T_0&& ls_0, T_1&& ls_1 = 1, T_2&& ls_2 = 1 )
{
    return std::make_tuple( wrapper<T_0&&>( std::forward<T_0>(ls_0) ), wrapper<T_1&&>( std::forward<T_1>(ls_1) ), wrapper<T_2&&>( std::forward<T_2>(ls_2) ) );
}


#endif

class device_info
{
public:
    enum device_t { CPU, GPU, ACC };

    device_info(const cl::Platform &platform, const cl::Device &device)
            : _platform(platform), _device(device)
    {}

    device_info( cl_device_id device_id )
            : _platform(), _device( cl::Device( device_id ) )
    {
        _platform = _device.getInfo<CL_DEVICE_PLATFORM>();
    }

    device_info( const int& platform_number, const int& device_number )
            : _platform(), _device(), _platform_id(platform_number), _device_id(device_number)
    {
    }

    device_info( const std::string& vendor_name,
                 const device_t&    device_type,
                 const int&         device_number,
                 const bool&        silent = false
    )
            : _platform(), _device()
    {
        cl_device_type ocl_device_type;
        switch( device_type )
        {
            case CPU:
                ocl_device_type = CL_DEVICE_TYPE_CPU;
                break;

            case GPU:
                ocl_device_type = CL_DEVICE_TYPE_GPU;
                break;

            case ACC:
                ocl_device_type = CL_DEVICE_TYPE_ACCELERATOR;
                break;

            default:
                assert( false && "unknown device type" );
                break;
        }

        // get platform
        std::vector<cl::Platform> platforms;

        bool found = false;
        auto error = cl::Platform::get( &platforms ); check_error( error );
        for( const auto& platform : platforms )
        {
            std::string platform_name;
            platform.getInfo( CL_PLATFORM_VENDOR, &platform_name );
            if( platform_name.find( vendor_name ) != std::string::npos )
            {
                _platform = platform;
                if (!silent) std::cout << "Platform with name " << platform_name << " found." << std::endl;
                found = true;
                break;
            }
        }


        if( !found )
        {
            std::cout << "Platform not found." << std::endl;
            exit( 1 );
        }

        // get device
        std::vector<cl::Device> devices;
        _platform.getDevices( ocl_device_type, &devices );

        if( device_number < devices.size() )
        {
            _device = devices[ device_number ];
            std::string device_name;
            _device.getInfo( CL_DEVICE_NAME, &device_name );
            if (!silent) std::cout << "Device with name " << device_name << " found." << std::endl;
        }
        else
        {
            std::cout << "Device not found." << std::endl;
            exit( 1 );
        }
    }

    cl::Platform platform() const
    {
        return _platform;
    }

    size_t platform_id() const
    {
        return _platform_id;
    }


    cl::Device device() const
    {
        return _device;
    }

    size_t device_id() const
    {
        return _device_id;
    }

    void initialize(bool silent = false)
    {
        // get platform
        std::vector<cl::Platform> platforms;
        cl::Platform::get(&platforms);
        if( _platform_id < platforms.size() )
        {
            _platform = platforms[_platform_id];
            std::string platform_name;
            _platform.getInfo( CL_PLATFORM_VENDOR, &platform_name );
            if (!silent) std::cout << "Platform with name " << platform_name << " found." << std::endl;
        }
        else
        {
            std::cout << "Platform not found." << std::endl;
            exit( 1 );
        }

        // get device
        std::vector<cl::Device> devices;
        _platform.getDevices( CL_DEVICE_TYPE_ALL, &devices );

        if( _device_id < devices.size() )
        {
            _device = devices[ _device_id ];
            std::string device_name;
            _device.getInfo( CL_DEVICE_NAME, &device_name );
            if (!silent) std::cout << "Device with name " << device_name << " found." << std::endl;
        }
        else
        {
            std::cout << "Device not found." << std::endl;
            exit( 1 );
        }
    }


private:
    cl::Platform _platform;
    cl::Device   _device;

    size_t _platform_id;
    size_t _device_id;
};

class kernel_info
{
public:
    enum SOURCE_TYPE {CODE, FILENAME};

    kernel_info( SOURCE_TYPE type, std::string source, std::string name = "func", std::string flags = "" )
            : _type(type), _source( source ), _name( name ), _flags( flags )
    {}

    SOURCE_TYPE type() const
    {
        return _type;
    }

    std::string source() const
    {
        return _source;
    }

    std::string name() const
    {
        return _name;
    }

    std::string flags() const
    {
        return _flags;
    }

private:
    SOURCE_TYPE _type;
    std::string _source;
    std::string _name;
    std::string _flags;
};


template<typename INT_RES_T,
        typename GS_0, typename GS_1, typename GS_2,
        typename LS_0, typename LS_1, typename LS_2,
        typename IS_VALID_CALLABLE,
        typename... Ts>
class ocl_wrapper_class {
    // helper
    cl_int error;
    cl_uint arg_index = 0;
    size_t buffer_pos = 0;
    unsigned long long best_runtime;
    std::chrono::time_point<std::chrono::system_clock> last_callback;

public:
    ocl_wrapper_class(const device_info &device,
                      const std::tuple<Ts...> &kernel_inputs,
                      const IS_VALID_CALLABLE &is_valid,

                      const kernel_info &kernel,
                      const std::tuple<GS_0, GS_1, GS_2> &global_size,
                      const std::tuple<LS_0, LS_1, LS_2> &local_size,
                      const size_t num_inputs,

                      const size_t num_evaluations,
                      const size_t warm_ups,

                      const bool silent,
                      const process_wrapper_info &wrapper,
                      const timeout warm_up_timeout,
                      const timeout evaluation_timeout,

                      const std::function<void(atf::configuration &, unsigned long long)> &callback,
                      const unsigned long long callback_interval) :
            _device_info(device), _context(), _command_queue(),
            _kernel_inputs(kernel_inputs),
            _is_valid(is_valid),
            _program(), _kernel_source(kernel.source()), _kernel_name(kernel.name()),
            _kernel_flags(kernel.flags()), _global_size_pattern(global_size),
            _local_size_pattern(local_size), _num_inputs(num_inputs), _kernel_buffers(),
            _kernel_input_sizes(),
            _num_evaluations(num_evaluations), _warm_ups(warm_ups), _callback(callback),
            _callback_interval(callback_interval), _wrapper(wrapper), _silent(silent), _gold_ptr(NULL),
            _warm_up_timeout(warm_up_timeout), _evaluation_timeout(evaluation_timeout) {
        if (_wrapper.type != NONE && kernel.type() != kernel_info::FILENAME) {
            std::cerr << "source code has to be submitted as file name when using remote tuning" << std::endl;
        }

        best_runtime = std::numeric_limits<unsigned long long>::max() / _num_evaluations;

        if (kernel.type() == kernel_info::FILENAME) {
            if (_wrapper.type == NONE) {
                std::ifstream in_file(kernel.source(), std::ifstream::in);
                _kernel_source = std::string((std::istreambuf_iterator<char>(in_file)),
                                              std::istreambuf_iterator<char>());
                in_file.close();
            } else {
                _kernel_source = kernel.source();
            }
        }

        this->read_input_sizes(_kernel_input_sizes, 0, _num_inputs, std::make_index_sequence<sizeof...(Ts)>());

        switch (wrapper.type) {
            case NONE: {
                _platform = device.platform();
                _device = device.device();
                init_open_cl();
                if (!wrapper.result_file_name.empty() && wrapper.result_file_name != "none") {
                    std::ifstream is(wrapper.result_file_name);
                    std::istream_iterator<INT_RES_T> start(is), end;
                    std::vector<INT_RES_T> expected_result(start, end);
                    INT_RES_T *gold_data = new INT_RES_T[expected_result.size()];
                    for (int i = 0; i < expected_result.size(); ++i) {
                        gold_data[i] = expected_result[i];
                    }
                    check_result(gold_data);
                }
                break;
            }
            case LOCAL: {
                _server_socket = socket(AF_UNIX, SOCK_STREAM, 0);
                if (_server_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = std::tmpnam(nullptr);
                struct sockaddr_un server_addr{};
                memset(&server_addr, 0, sizeof(struct sockaddr_un));
                server_addr.sun_family = AF_UNIX;
                strncpy(server_addr.sun_path, _server.c_str(), sizeof(server_addr.sun_path) - 1);
                if (bind(_server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                if (listen(_server_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
            case REMOTE: {
                _server_socket = socket(AF_INET, SOCK_STREAM, 0);
                if (_server_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in server_addr{};
                memset(&server_addr, 0, sizeof(server_addr));
                server_addr.sin_family = AF_INET;
                server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
                server_addr.sin_port = htons(0);
                if (bind(_server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in assigned_server_addr{};
                socklen_t socket_length = sizeof(server_addr);
                if (getsockname(_server_socket, (struct sockaddr *) &assigned_server_addr, &socket_length) < 0) {
                    std::cerr << "error while getting port: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = "localhost";
                _port = ntohs(assigned_server_addr.sin_port);
                if (listen(_server_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
        }

        if (!silent) std::cout << "_warm_ups: " << _warm_ups << std::endl;
        if (!silent) std::cout << "_num_evaluations: " << _num_evaluations << std::endl;

        last_callback = std::chrono::system_clock::now() - std::chrono::seconds(_callback_interval);
    }

    ~ocl_wrapper_class() {
        if (_wrapper.type == NONE && !_wrapper.result_file_name.empty() && _wrapper.result_file_name != "none") {
            delete[] (INT_RES_T*) _gold_ptr;
        }
    }

    void init_open_cl() {
        // delete existing open cl objects
        _kernel_buffers.clear();
        _kernel_buffer_sizes.clear();

        // create context and command queue
        cl_context_properties props[] = {CL_CONTEXT_PLATFORM,
                                         reinterpret_cast<cl_context_properties>( _platform()),
                                         0
        };

        _context = cl::Context(VECTOR_CLASS<cl::Device>(1, _device), props);
        _command_queue = cl::CommandQueue(_context, _device, CL_QUEUE_PROFILING_ENABLE);

#ifndef USE_CLANG_PREPROCESSOR
        // create programs
        _program = cl::Program(_context,
                               cl::Program::Sources(1, std::make_pair(_kernel_source.c_str(),
                                                                      _kernel_source.length()))
        );
#endif

        // create kernel input buffers
        this->create_buffers(_kernel_buffers, _kernel_buffer_sizes, 0, _num_inputs,
                             std::make_index_sequence<sizeof...(Ts)>());
    }

    void warm_ups(size_t warm_ups) {
        _warm_ups = warm_ups;
    }

    void evaluations(size_t evaluations) {
        _num_evaluations = evaluations;
    }

    unsigned long long operator()(configuration &configuration, std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
                      unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        if (_callback && std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now() - last_callback).count() >= _callback_interval) {
            _callback(configuration, best_runtime);
            last_callback = std::chrono::system_clock::now();
        }

        // update tp values
        for (auto &tp : configuration) {
            auto tp_value = tp.second;
            tp_value.update_tp();
//            if (!_silent && tp.first.find("NOT_USED") == std::string::npos)
//                std::cout << tp.first << "\t" << tp.second.value().size_t_val()<< std::endl;
        }

        if (!_is_valid(configuration)) {
            throw std::exception();
        }

        // calculate timeouts
        unsigned long warm_up_timeout = 0;
        if (_warm_up_timeout.type == ABSOLUTE) {
            warm_up_timeout = _warm_up_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _warm_up_timeout.value.factor) {
            warm_up_timeout = static_cast<unsigned long>(_warm_up_timeout.value.factor * best_runtime);
        }
        unsigned long evaluation_timeout = 0;
        if (_evaluation_timeout.type == ABSOLUTE) {
            evaluation_timeout = _evaluation_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _evaluation_timeout.value.factor) {
            evaluation_timeout = static_cast<unsigned long>(_evaluation_timeout.value.factor * best_runtime);
        }

        // get global and local size
        size_t gs_0 = std::get<0>(_global_size_pattern).get_value();
        size_t gs_1 = std::get<1>(_global_size_pattern).get_value();
        size_t gs_2 = std::get<2>(_global_size_pattern).get_value();

        size_t ls_0 = std::get<0>(_local_size_pattern).get_value();
        size_t ls_1 = std::get<1>(_local_size_pattern).get_value();
        size_t ls_2 = std::get<2>(_local_size_pattern).get_value();

        // create flags
        std::stringstream flags;

        for (const auto &tp : configuration)
            flags << " -D " << tp.second.name() << "=" << tp.second.value();
        // set additional kernel flags for first kernel
        flags << _kernel_flags;

        if (runtimes != nullptr) runtimes->resize(1);

        unsigned long long runtime;
        if (_wrapper.type == NONE) {
            runtime = direct_execution(
                    warm_up_timeout, evaluation_timeout, configuration,
                    gs_0, gs_1, gs_2,
                    ls_0, ls_1, ls_2,
                    flags, runtimes, compile_time, error_code
            );
        } else {
            runtime = seperate_process_execution(
                    warm_up_timeout, evaluation_timeout, configuration,
                    gs_0, gs_1, gs_2,
                    ls_0, ls_1, ls_2,
                    flags, runtimes, compile_time, error_code
            );
        }
        if (runtime < best_runtime) {
            best_runtime = runtime;
            if (_callback) {
                _callback(configuration, runtime);
                last_callback = std::chrono::system_clock::now();
            }
        }
        return runtime;
    }

    unsigned long long direct_execution(unsigned long warm_up_timeout, unsigned long evaluation_timeout, configuration &configuration,
                            size_t gs_0, size_t gs_1, size_t gs_2,
                            size_t ls_0, size_t ls_1, size_t ls_2,
                            const std::stringstream &flags, std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
                            unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        // compile kernel
        try {
#ifdef USE_CLANG_PREPROCESSOR
            auto start_preprocessing = std::chrono::system_clock::now();
            std::string preprocessed_source;
            preprocessed_source.reserve(_kernel_source.length());
            preprocess(_kernel_source, preprocessed_source, flags.str());
            _program = cl::Program(_context,
                                   cl::Program::Sources(1, std::make_pair(preprocessed_source.c_str(),
                                                                          preprocessed_source.length()))
            );
            auto end_preprocessing = std::chrono::system_clock::now();
            std::cout << "preprocessing time for kernel: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_preprocessing - start_preprocessing).count() << "ms" << std::endl;
#endif
            auto start = std::chrono::system_clock::now();
            _program.build(std::vector<cl::Device>(1, _device), flags.str().c_str());
            auto end = std::chrono::system_clock::now();

            auto buildLog = _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device);
//                if (buildLog.length() > 0) {
//                    std::cout << std::endl << "Build of first kernel successful! Log:" << std::endl << buildLog
//                              << std::endl;
//                }

            auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            std::cout << "compilation time for kernel: " << runtime_in_sec << "ms" << std::endl;
            if (compile_time != nullptr) *compile_time += runtime_in_sec;
        } catch (cl::Error &err) {
            std::cout << flags.str() << std::endl;
            std::cout << err.err() << std::endl;
            if (error_code != nullptr) *error_code = err.err();
            if (err.err() == CL_BUILD_PROGRAM_FAILURE) {
                auto buildLog = _program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device);
                std::cout << std::endl << "Build of kernel failed! Log:" << std::endl << buildLog << std::endl;
            }

            throw std::exception();
        }
        auto kernel = cl::Kernel(_program, _kernel_name.c_str(), &error);
        check_error(error); // TODO: code aus PJS WS16/16 übernehmen für automatische Erkennung "func"

        // save program binary
#if (SAVE_BINARY != 0)  // Intel: objdump -M Intel -D -m i386 -b binary program_binary
        {
            size_t binary_size;

            // allocate memory
            clGetProgramInfo(_program(), CL_PROGRAM_BINARY_SIZES, sizeof(binary_size), &binary_size, NULL);
            char* binary = (char*) malloc( sizeof(char) * binary_size );

            // get binary
            clGetProgramInfo(_program(), CL_PROGRAM_BINARIES, sizeof(char) * binary_size , &binary, NULL);

            // save binary in file
            std::ofstream file;
            file.open("binary_program_kernel");
            file.write( binary, binary_size );
            file.close();

            // free memory
            free(binary);
        }
#endif

        // set kernel arguments for first kernel
        arg_index = 0;
        buffer_pos = 0;
        set_kernel_args(kernel, _kernel_buffers, 0, _kernel_input_sizes.size(),
                        std::make_index_sequence<sizeof...(Ts)>());

        // start first kernel
        cl::Event event;
        cl::NDRange global_size(gs_0, gs_1, gs_2);
        cl::NDRange local_size(ls_0, ls_1, ls_2);

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            cl::Event timeout_event;
            error = _command_queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size, NULL,
                                                        &timeout_event);
            check_error(error);
            error = wait_for_event(timeout_event, warm_up_timeout);
            check_error(error);
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            error = _command_queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size, NULL,
                                                        &event);
            check_error(error);
            error = event.wait();
            check_error(error);
        }

        // kernel launch with profiling
        cl_ulong kernel_runtime_in_ns = 0;
        cl_ulong start_time;
        cl_ulong end_time;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            cl::Event timeout_event;
            error = _command_queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size, NULL,
                                                        &timeout_event);
            error = wait_for_event(timeout_event, evaluation_timeout);
            check_error(error);

            error = timeout_event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start_time);
            check_error(error);
            error = timeout_event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end_time);
            check_error(error);

            cl_ulong runtime = end_time - start_time;
            if (runtimes != nullptr) (*runtimes)[0].push_back(runtime);
            kernel_runtime_in_ns += runtime;

            if (runtimes == nullptr && _num_evaluations > 1 &&
                kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                if (_gold_ptr != NULL) check_results_helper(1, configuration);
                return kernel_runtime_in_ns;
            }
        }
        for (size_t i = 1; i < _num_evaluations; ++i) {
            error = _command_queue.enqueueNDRangeKernel(kernel, cl::NullRange, global_size, local_size, NULL,
                                                        &event);
            error = event.wait();
            check_error(error);

            error = event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start_time);
            check_error(error);
            error = event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end_time);
            check_error(error);

            cl_ulong runtime = end_time - start_time;
            if (runtimes != nullptr) (*runtimes)[0].push_back(runtime);
            kernel_runtime_in_ns += runtime;

            if (runtimes == nullptr && _num_evaluations > 1 &&
                kernel_runtime_in_ns > best_runtime * _num_evaluations) {
                if (_gold_ptr != NULL) check_results_helper(1, configuration);
                return kernel_runtime_in_ns / (i + 1);
            }
        }
        kernel_runtime_in_ns /= _num_evaluations;
        if (_gold_ptr != NULL) check_results_helper(1, configuration);
        return kernel_runtime_in_ns;
    }

    unsigned long long seperate_process_execution(unsigned long warm_up_timeout, unsigned long evaluation_timeout,
                                      configuration &configuration,
                                      size_t gs_0, size_t gs_1, size_t gs_2,
                                      size_t ls_0, size_t ls_1, size_t ls_2,
                                      const std::stringstream &flags,
                                      std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
                                      unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        addr client_addr{};
        int client_socket = -1;
        socklen_t client_addr_length = _wrapper.type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in);
        auto wait_for_connection = [&]() {
            struct sockaddr *addr_ptr = nullptr;
            if (_wrapper.type == LOCAL) {
                addr_ptr = (struct sockaddr *) &client_addr.addr_un;
            } else {
                addr_ptr = (struct sockaddr *) &client_addr.addr_in;
            }
            client_socket = accept(_server_socket, addr_ptr, &client_addr_length);
            if (client_socket < 0) {
                std::cerr << "error while accepting client connection: " << strerror(errno) << std::endl;
                exit(1);
            }
        };
        std::thread wait_for_connection_thread(wait_for_connection);

        std::string command = _wrapper.command_prefix;
        if (_wrapper.type == REMOTE) {
            size_t ssh_pos = command.find("ssh ");
            if (ssh_pos != std::string::npos) {
                // enable port forwarding in case of remote tuning
                command.insert(ssh_pos + 3, std::string(" -R ").append(_server).append(":").append(std::to_string(_port)).append(":").append(_server).append(":").append(std::to_string(_port)));
            }
        }
        command.append(" --platform-id ").append(std::to_string(_device_info.platform_id()));
        command.append(" --device-id ").append(std::to_string(_device_info.device_id()));
        command.append(" --routine-name ").append(_kernel_name);
        command.append(" --source-file \"").append(_kernel_source).append("\"");
        if (_wrapper.type == LOCAL) {
            command.append(" --flags \"").append(flags.str()).append("\"");
        } else {
            command.append(" --flags \\\"").append(flags.str()).append("\\\"");
        }
        command.append(" --kernel-input-sizes");
        make_process_wrapper_input_sizes_flags(command, 0, _num_inputs, std::make_index_sequence<sizeof...(Ts)>());
        command.append(" --kernel-global-size ")
                .append(std::to_string(gs_0)).append(" ")
                .append(std::to_string(gs_1)).append(" ")
                .append(std::to_string(gs_2));
        command.append(" --kernel-local-size ")
                .append(std::to_string(ls_0)).append(" ")
                .append(std::to_string(ls_1)).append(" ")
                .append(std::to_string(ls_2));

        if (_wrapper.type == LOCAL) {
            command.append(" --server-type local");
            command.append(" --server ").append("\"").append(_server).append("\"");
            command.append(" --port 0");
        } else {
            command.append(" --server-type remote");
            command.append(" --server ").append("\"").append(_server).append("\"");
            command.append(" --port ").append(std::to_string(_port));
        }
        command.append(" --warm-ups ").append(std::to_string(_warm_ups));
        command.append(" --evaluations ").append(std::to_string(_num_evaluations));
        command.append(" --warm-up-timeout ").append(std::to_string(warm_up_timeout));
        command.append(" --evaluation-timeout ").append(std::to_string(evaluation_timeout));
        command.append(" --expected-result-file ").append("\"").append(_wrapper.result_file_name).append("\"");
        command.append(" --benchmark ");
        if (runtimes != nullptr) {
            command.append("true");
        } else {
            command.append("false");
        }
        command.append(" --buffer-fill-algorithm \"").append(_wrapper.buffer_fill_algorithm).append("\"");

//        std::cout << command << std::endl;

        // execute process wrapper
        int ret_val;
        std::atomic_bool cancel(false);
        auto exec_command = [&]() {
            ret_val = system(command.c_str());

            if (client_socket < 0) {
                // process wrapper could not connect before failing
                // cancel waiting for connection by connecting to server socket locally
                cancel = true;
                int cancel_client_socket = socket(_wrapper.type == LOCAL ? AF_UNIX : AF_INET, SOCK_STREAM, 0);
                if (cancel_client_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }

                atf::cf::addr server_addr{};
                if (_wrapper.type == LOCAL) {
                    server_addr.addr_un.sun_family = AF_UNIX;
                    strncpy(server_addr.addr_un.sun_path, _server.c_str(), sizeof(server_addr.addr_un.sun_path) - 1);
                } else {
                    memset(&server_addr.addr_in, 0, sizeof(server_addr.addr_in));
                    server_addr.addr_in.sin_family = AF_INET;
                    server_addr.addr_in.sin_port = htons(_port);
                    struct hostent *hostinfo;
                    hostinfo = gethostbyname(_server.c_str());
                    if (hostinfo == nullptr) {
                        std::cerr << "invalid server: " << _server << std::endl;
                        exit(1);
                    }
                    server_addr.addr_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;
                }

                struct sockaddr *addr_ptr = nullptr;
                if (_wrapper.type == LOCAL) {
                    addr_ptr = (struct sockaddr *) &server_addr.addr_un;
                } else {
                    addr_ptr = (struct sockaddr *) &server_addr.addr_in;
                }
                if (connect(cancel_client_socket, addr_ptr,
                            _wrapper.type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in)) < 0) {
                    std::cerr << "error while connecting to server: " << strerror(errno) << std::endl;
                    exit(1);
                }
                if (close(cancel_client_socket) != 0) {
                    std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")"
                              << std::endl;
                    exit(1);
                }
            }
        };
        std::thread exec_command_thread(exec_command);

        // wait for client connection
        wait_for_connection_thread.join();

        // if process wrapper failed, throw exception
        if (cancel) {
            exec_command_thread.join();
            // close client connection
            if (close(client_socket) != 0) {
                std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")"
                          << std::endl;
                exit(1);
            }
            throw std::exception();
        }

        // determine what values to receive
        uint16_t mask_buf = 0;
        auto mask_data = (char *) (&mask_buf);
        size_t left = sizeof(mask_buf);
        ssize_t rc;
        do {
            rc = recv(client_socket, mask_data, left, 0);
            if (rc < 0) {
                std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")" << std::endl;
                exit(1);
            }
            mask_data += rc;
            left -= rc;
        } while (left > 0);
        short recv_mask = htons(mask_buf);
        auto recv_compile_time = static_cast<bool>(recv_mask & 1);
        auto recv_error_code = static_cast<bool>(recv_mask & 2);
        auto recv_runtimes = static_cast<bool>(recv_mask & 4);

        if (recv_compile_time) {
            // read compile time
            __uint64_t buf;
            auto data = (char *) (&buf);
            left = sizeof(buf);
            do {
                rc = recv(client_socket, data, left, 0);
                if (rc < 0) {
                    std::cerr << "error while receiving compile time: " << strerror(errno) << "(" << errno << ")"
                              << std::endl;
                    exit(1);
                }
                data += rc;
                left -= rc;
            } while (left > 0);
            if (compile_time != nullptr) *compile_time = be64toh(buf);
        }

        if (recv_error_code) {
            // read error code
            uint32_t buf;
            auto data = (char *) (&buf);
            left = sizeof(buf);
            do {
                rc = recv(client_socket, data, left, 0);
                if (rc < 0) {
                    std::cerr << "error while receiving error code: " << strerror(errno) << "(" << errno << ")"
                              << std::endl;
                    exit(1);
                }
                data += rc;
                left -= rc;
            } while (left > 0);
            if (error_code != nullptr) {
                *error_code = ntohl(buf);
            }
            data = (char *) (&buf);
            left = sizeof(buf);
            do {
                rc = recv(client_socket, data, left, 0);
                if (rc < 0) {
                    std::cerr << "error while receiving sign of error code: " << strerror(errno) << "(" << errno << ")"
                              << std::endl;
                    exit(1);
                }
                data += rc;
                left -= rc;
            } while (left > 0);
            if (error_code != nullptr) {
                if (ntohl(buf) != 0) {
                    *error_code *= -1;
                }
            }
        }

        unsigned long long runtime = 0;
        if (recv_runtimes) {
            // read client runtime
            __uint64_t buf;
            auto data = (char *) (&buf);
            left = sizeof(buf);
            do {
                rc = recv(client_socket, data, left, 0);
                if (rc < 0) {
                    std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")"
                              << std::endl;
                    exit(1);
                }
                data += rc;
                left -= rc;
            } while (left > 0);
            runtime = be64toh(buf);

            if (runtimes != nullptr) {
                // when benchmarking, receive all runtimes
                std::vector<size_t> kernel_ids = {0};
                for (size_t id : kernel_ids) {
                    for (int i = 0; i < _num_evaluations; ++i) {
                        data = (char *) (&buf);
                        left = sizeof(buf);
                        do {
                            rc = recv(client_socket, data, left, 0);
                            if (rc < 0) {
                                std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")"
                                          << std::endl;
                                exit(1);
                            }
                            data += rc;
                            left -= rc;
                        } while (left > 0);
                        (*runtimes)[id].push_back(be64toh(buf));
                    }
                }
            }
        }

        // close client connection
        if (close(client_socket) != 0) {
            std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")" << std::endl;
            exit(1);
        }


        // wait for process wrapper to terminate
        exec_command_thread.join();

        if (!recv_runtimes) throw std::exception();
        return runtime;
    }

    void check_result(void *gold_ptr) {
        _gold_ptr = gold_ptr;
    }

private:
    const IS_VALID_CALLABLE &_is_valid;

    device_info _device_info;
    cl::Platform _platform;
    cl::Device _device;
    cl::Context _context;
    cl::CommandQueue _command_queue;

    std::tuple<Ts...> _kernel_inputs;

    // Kernel
    cl::Program _program;
    std::string _kernel_source;
    std::string _kernel_name;
    std::string _kernel_flags;
    std::tuple<GS_0, GS_1, GS_2> _global_size_pattern;
    std::tuple<LS_0, LS_1, LS_2> _local_size_pattern;
    size_t _num_inputs;
    std::vector<cl::Buffer> _kernel_buffers;
    std::vector<size_t> _kernel_buffer_sizes;
    std::vector<size_t> _kernel_input_sizes;

    size_t _num_evaluations;
    size_t _warm_ups;

    const std::function<void(atf::configuration &, unsigned long long)> _callback;
    const unsigned long long _callback_interval;

    const process_wrapper_info _wrapper;
    int _server_socket;
    std::string _server;
    unsigned short int _port;

    const bool _silent;
    const timeout _warm_up_timeout;
    const timeout _evaluation_timeout;

    void *_gold_ptr;

    // helper for creating arguments for process wrapper
    template<size_t... Is>
    void make_process_wrapper_input_sizes_flags(std::string &flags,
                                                size_t offset, size_t count,
                                                std::index_sequence<Is...>) {
        make_process_wrapper_input_sizes_flags_impl(flags,
                                                    offset, count,
                                                    std::get<Is>(_kernel_inputs)...);
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     scalar<int> &scalar, ARGs &... args) {
        if (offset == 0) {
            std::stringstream ss;
            ss << std::fixed << std::setprecision(std::numeric_limits<int>::max_digits10) << scalar.get();
            flags.append(" ").append("si ").append(ss.str());

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     scalar<float> &scalar, ARGs &... args) {
        if (offset == 0) {
            std::stringstream ss;
            ss << std::fixed << std::setprecision(std::numeric_limits<float>::max_digits10) << scalar.get();
            flags.append(" ").append("sf ").append(ss.str());

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     scalar<double> &scalar, ARGs &... args) {
        if (offset == 0) {
            std::stringstream ss;
            ss << std::fixed << std::setprecision(std::numeric_limits<double>::max_digits10) << scalar.get();
            flags.append(" ").append("sd ").append(ss.str());

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     scalar<size_t> &scalar, ARGs &... args) {
        if (offset == 0) {
            flags.append(" ").append("sst ").append(std::to_string(scalar.get()));

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     buffer_class<float> &buffer, ARGs &... args) {
        if (offset == 0) {
            flags.append(" ").append("wbf ").append(std::to_string(buffer.size()));

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     buffer_class<double> &buffer, ARGs &... args) {
        if (offset == 0) {
            flags.append(" ").append("wbd ").append(std::to_string(buffer.size()));

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     buffer_class<size_t> &buffer, ARGs &... args) {
        if (offset == 0) {
            flags.append(" ").append("wbst ").append(std::to_string(buffer.size()));

            if (count > 1) {
                make_process_wrapper_input_sizes_flags_impl(flags,
                                                            offset, count - 1,
                                                            args...);
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    template<typename... ARGs>
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
                                                     size_t offset, size_t count,
                                                     const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                size_t elem_count;
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                        flags.append(" ").append("lbf ").append(runtime_input._value);
                        break;
                    case LINEAR_BUFFER_DOUBLE:
                        flags.append(" ").append("lbd ").append(runtime_input._value);
                        break;
                    case LINEAR_BUFFER_SIZE_T:
                        flags.append(" ").append("lbst ").append(runtime_input._value);
                        break;
                    case WRAP_BUFFER_FLOAT:
                        flags.append(" ").append("wbf ").append(runtime_input._value);
                        break;
                    case WRAP_BUFFER_DOUBLE:
                        flags.append(" ").append("wbd ").append(runtime_input._value);
                        break;
                    case WRAP_BUFFER_SIZE_T:
                        flags.append(" ").append("wbst ").append(runtime_input._value);
                        break;
                    case SCALAR_INT:
                        flags.append(" ").append("si ").append(runtime_input._value);
                        break;
                    case SCALAR_FLOAT:
                        flags.append(" ").append("sf ").append(runtime_input._value);
                        break;
                    case SCALAR_DOUBLE:
                        flags.append(" ").append("sd ").append(runtime_input._value);
                        break;
                    case SCALAR_SIZE_T:
                        flags.append(" ").append("sst ").append(runtime_input._value);
                        break;
                }
            }
        } else {
            make_process_wrapper_input_sizes_flags_impl(flags,
                                                        offset - 1, count,
                                                        args...);
        }
    }
    void make_process_wrapper_input_sizes_flags_impl(std::string &flags, size_t offset, size_t count) {}

    // helper for reading input sizes
    template<size_t... Is>
    void read_input_sizes(std::vector<size_t> &_kernel_input_sizes,
                          size_t offset, size_t count,
                          std::index_sequence<Is...>) {
        read_input_sizes_impl(_kernel_input_sizes,
                              offset, count,
                              std::get<Is>(_kernel_inputs)...);
    }

    template<typename T, typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t> &_kernel_input_sizes,
                               size_t offset, size_t count,
                               const scalar <T> &scalar, ARGs &... args) {
        if (offset == 0) {
            _kernel_input_sizes.emplace_back(1);

            if (count > 1) {
                read_input_sizes_impl(_kernel_input_sizes,
                                      offset, count - 1,
                                      args...);
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    template<typename T, typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t> &_kernel_input_sizes,
                               size_t offset, size_t count,
                               const buffer_class <T> &buffer, ARGs &... args) {
        if (offset == 0) {
            // add buffer size to _kernel_input_sizes
            _kernel_input_sizes.emplace_back(buffer.size());

            if (count > 1) {
                read_input_sizes_impl(_kernel_input_sizes,
                                      offset, count - 1,
                                      args...);
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    template<typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t> &_kernel_input_sizes,
                               size_t offset, size_t count,
                               const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        // add buffer size to _kernel_input_sizes
                        size_t input_size;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> input_size)) {
                            std::cerr << "unable to parse buffer size " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        _kernel_input_sizes.emplace_back(input_size);
                        break;
                    }
                    case SCALAR_INT:
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // add scalar size (1) to _kernel_input_sizes
                        _kernel_input_sizes.emplace_back(1);
                        break;
                }
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    void read_input_sizes_impl(std::vector<size_t> &_kernel_input_sizes,
                               size_t offset, size_t count) {}

    // helper for creating buffers
    template<size_t... Is>
    void create_buffers(std::vector<cl::Buffer> &_kernel_buffers,
                        std::vector<size_t> &_kernel_buffer_sizes,
                        size_t offset, size_t count,
                        std::index_sequence<Is...>) {
        create_buffers_impl(_kernel_buffers,
                            _kernel_buffer_sizes,
                            offset, count,
                            std::get<Is>(_kernel_inputs)...);
    }

    template<typename T, typename... ARGs>
    void create_buffers_impl(std::vector<cl::Buffer> &_kernel_buffers,
                             std::vector<size_t> &_kernel_buffer_sizes,
                             size_t offset, size_t count,
                             const scalar <T> &scalar, ARGs &... args) {
        if (offset == 0) {
            if (count > 1) {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_buffer_sizes,
                                    offset, count - 1,
                                    args...);
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    template<typename T, typename... ARGs>
    void create_buffers_impl(std::vector<cl::Buffer> &_kernel_buffers,
                             std::vector<size_t> &_kernel_buffer_sizes,
                             size_t offset, size_t count,
                             const buffer_class <T> &buffer, ARGs &... args) {
        if (offset == 0) {
            auto start_time = std::chrono::system_clock::now();

            // add buffer size to _kernel_input_sizes
            _kernel_buffer_sizes.emplace_back(buffer.size() * sizeof(T));

            // create buffer
            _kernel_buffers.emplace_back(_context, CL_MEM_READ_WRITE, buffer.size() * sizeof(T));

            try {
                error = _command_queue.enqueueWriteBuffer(_kernel_buffers.back(), CL_TRUE,
                                                          0, _kernel_buffer_sizes.back(),
                                                          buffer.get());
                check_error(error);
            }
            catch (cl::Error &err) {
                std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
                abort();
            }

            auto end_time = std::chrono::system_clock::now();
            auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
            if (!_silent) std::cout << "Time to create and fill buffer: " << runtime << "ms" << std::endl;

            if (count > 1) {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_buffer_sizes,
                                    offset, count - 1,
                                    args...);
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    template<typename... ARGs>
    void create_buffers_impl(std::vector<cl::Buffer> &_kernel_buffers,
                             std::vector<size_t> &_kernel_buffer_sizes,
                             size_t offset, size_t count,
                             const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                size_t elem_count;
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        // get element count
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> elem_count)) {
                            std::cerr << "unable to parse element count " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        break;
                    }
                    case SCALAR_INT:
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // skip scalar inputs
                        continue;
                }

                // get element size
                size_t elem_size;
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case WRAP_BUFFER_FLOAT:
                        elem_size = sizeof(float);
                        break;
                    case LINEAR_BUFFER_DOUBLE:
                    case WRAP_BUFFER_DOUBLE:
                        elem_size = sizeof(double);
                        break;
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_SIZE_T:
                        elem_size = sizeof(size_t);
                        break;
                    case SCALAR_INT:
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // skip scalar inputs
                        continue;
                }

                auto start_time = std::chrono::system_clock::now();

                void *data = malloc(elem_count * elem_size);
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                        for (int i = 0; i < elem_count; ++i) ((float *) data)[i] = i;
                        break;
                    case WRAP_BUFFER_FLOAT:
                        for (int i = 0; i < elem_count; ++i) ((float *) data)[i] = (i % 10) + 1;
                        break;
                    case LINEAR_BUFFER_DOUBLE:
                        for (int i = 0; i < elem_count; ++i) ((double *) data)[i] = i;
                        break;
                    case WRAP_BUFFER_DOUBLE:
                        for (int i = 0; i < elem_count; ++i) ((double *) data)[i] = (i % 10) + 1;
                        break;
                    case LINEAR_BUFFER_SIZE_T:
                        for (int i = 0; i < elem_count; ++i) ((size_t *) data)[i] = static_cast<size_t>(i);
                        break;
                    case WRAP_BUFFER_SIZE_T:
                        for (int i = 0; i < elem_count; ++i) ((size_t *) data)[i] = static_cast<size_t>((i % 10) + 1);
                        break;
                    case SCALAR_INT:
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // skip scalar inputs
                        continue;
                }

                // add buffer size to _kernel_input_sizes
                _kernel_buffer_sizes.emplace_back(elem_count * elem_size);

                // create buffer
                _kernel_buffers.emplace_back(_context, CL_MEM_READ_WRITE, elem_count * elem_size);

                try {
                    error = _command_queue.enqueueWriteBuffer(_kernel_buffers.back(), CL_TRUE,
                                                              0, _kernel_buffer_sizes.back(),
                                                              data);
                    check_error(error);
                }
                catch (cl::Error &err) {
                    std::cerr << "ERROR: " << err.what() << "(" << err.err() << ")" << std::endl;
                    abort();
                }

                free(data);

                auto end_time = std::chrono::system_clock::now();
                auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
                if (!_silent) std::cout << "Time to create and fill buffer: " << runtime << "ms" << std::endl;
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    void create_buffers_impl(std::vector<cl::Buffer> &_kernel_buffers,
                             std::vector<size_t> &_kernel_buffer_sizes,
                             size_t offset, size_t count) {}


    // helper for set kernel arguments
    template<size_t... Is>
    void set_kernel_args(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count,
                         std::index_sequence<Is...>) {
        set_kernel_args_impl(kernel, _kernel_buffers, offset, count, std::get<Is>(_kernel_inputs)...);
    }

    template<typename T, typename... ARGs>
    void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count,
                              scalar <T> scalar, ARGs... args) {
        if (offset == 0) {
            kernel.setArg(arg_index++, scalar.get());

            if (count > 1) {
                set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
            }
        } else {
            set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
        }
    }

    template<typename T, typename... ARGs>
    void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count,
                              buffer_class <T> buffer, ARGs... args) {
        if (offset == 0) {
            kernel.setArg(arg_index++, _kernel_buffers[buffer_pos++]);

            if (count > 1) {
                set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
            }
        } else {
            set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
        }
    }

    template<typename... ARGs>
    void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count,
                              cl_mem buffer, ARGs... args) {
        if (offset == 0) {
            kernel.setArg(arg_index++, _kernel_buffers[buffer_pos++]);

            if (count > 1) {
                set_kernel_args_impl(kernel, _kernel_buffers, offset, count - 1, args...);
            }
        } else {
            set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
        }
    }

    template<typename... ARGs>
    void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count,
                              const std::vector<runtime_input> &runtime_inputs, ARGs... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        kernel.setArg(arg_index++, _kernel_buffers[buffer_pos++]);
                        break;
                    }
                    case SCALAR_INT: {
                        // parse scalar
                        int value;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> value)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        kernel.setArg(arg_index++, value);
                        break;
                    }
                    case SCALAR_FLOAT: {
                        // parse scalar
                        float value;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> value)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        kernel.setArg(arg_index++, value);
                        break;
                    }
                    case SCALAR_DOUBLE: {
                        // parse scalar
                        double value;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> value)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        kernel.setArg(arg_index++, value);
                        break;
                    }
                    case SCALAR_SIZE_T: {
                        // parse scalar
                        size_t value;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> value)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        kernel.setArg(arg_index++, value);
                        break;
                    }
                }
            }
        } else {
            set_kernel_args_impl(kernel, _kernel_buffers, offset - 1, count, args...);
        }
    }

    void set_kernel_args_impl(cl::Kernel &kernel, std::vector<cl::Buffer> &_kernel_buffers, size_t offset, size_t count) {}

    void check_results_helper(int kernel, configuration &config) {
        size_t result_buffer_size = _kernel_buffer_sizes.back();
        void *dev_result = malloc(result_buffer_size);
        error = _command_queue.enqueueReadBuffer(_kernel_buffers.back(), CL_TRUE, 0, result_buffer_size, dev_result);
        check_error(error);
        if (memcmp(dev_result, _gold_ptr, result_buffer_size)) {
            std::ofstream log_file;
            log_file.open("errors.txt", std::ofstream::out | std::ofstream::app);
            log_file << "!!! ERROR IN RESULT !!!" << std::endl;
            log_file << "configuration: " << std::endl;
            for (const auto &val : config) {
                log_file << "#define " << val.first << " " << val.second.value() << std::endl;
            }
            log_file << "result buffer: " << std::endl;
            for (int k = 0; k < result_buffer_size / sizeof(INT_RES_T); ++k) {
                log_file << ((INT_RES_T *) dev_result)[k] << "\t";
            }
            log_file << std::endl << "gold buffer: " << std::endl;
            for (int k = 0; k < result_buffer_size / sizeof(INT_RES_T); ++k) {
                log_file << ((INT_RES_T *) _gold_ptr)[k] << "\t";
            }
            log_file << std::endl;
            log_file.close();
//                throw std::exception();
            std::cout << "result is incorrect" << std::endl;
            exit(1);
        } else {
            std::cout << "result is correct" << std::endl;
        }
        free(dev_result);
    }

    cl_int wait_for_event(const cl::Event &event, unsigned long long timeout = 0) {
        if (timeout > 0) {
            auto timeout_lambda = [this, event, timeout]() {
                ns_sleep(timeout);

                cl_int status = -1;
                cl_int error = event.getInfo(CL_EVENT_COMMAND_EXECUTION_STATUS, &status);
                if (error != CL_INVALID_EVENT && status != CL_COMPLETE) {
                    // kernel still running
                    std::cerr << "kernel took more than " << timeout << " ns, killing process" << std::endl;
                    exit(1);
                }
            };
            std::thread timeout_thread(timeout_lambda);

            auto error = event.wait();
            timeout_thread.detach();
            return error;
        } else {
            return event.wait();
        }
    }

};


    template<typename INT_RES_T,
            typename GS_0, typename GS_1, typename GS_2,
            typename LS_0, typename LS_1, typename LS_2,
            typename IS_VALID_CALLABLE,
            typename... Ts>
    auto ocl(const device_info&                        device,

             const kernel_info&                        kernel,
             const std::tuple<Ts...>&                  kernel_inputs,
             const std::tuple<GS_0, GS_1, GS_2>&       global_size,
             const std::tuple<LS_0, LS_1, LS_2>&       local_size,

             const IS_VALID_CALLABLE&                  is_valid,

             const size_t                              num_evaluations,
             const size_t                              warm_ups,

             const bool                                silent = false,
             const process_wrapper_info&               wrapper = {NONE, "", "", ""},
             const timeout                             warm_up_timeout = {ABSOLUTE, {.absolute = 0}},
             const timeout                             evaluation_timeout = {ABSOLUTE, {.absolute = 0}},

             const std::function<void(atf::configuration&, unsigned long long)> &callback = {},
             const unsigned long long                              callback_interval = 100) {
        return ocl_wrapper_class<INT_RES_T, GS_0, GS_1, GS_2, LS_0, LS_1, LS_2, IS_VALID_CALLABLE, Ts...>
                (device, kernel_inputs,
                 is_valid,
                 kernel, global_size, local_size, sizeof...(Ts),
                 num_evaluations, warm_ups, silent, wrapper, warm_up_timeout, evaluation_timeout, callback, callback_interval);
    }
}
}

#endif //OCL_WRAPPER_HPP
