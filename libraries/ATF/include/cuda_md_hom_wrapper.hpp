//
// Created by  on 13.05.2017.
//

#ifndef MD_HOM_CUDA_MD_HOM_WRAPPER_HPP
#define MD_HOM_CUDA_MD_HOM_WRAPPER_HPP

#include <cuda.h>
#include <cuda_runtime.h>
#include <nvrtc.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>

#include <cstddef>
#include <tuple>
#include <chrono>
#include <unistd.h> // for sleep

#include "helper.hpp"
#include "ocl_wrapper.hpp"
#include "process_wrapper_helper.hpp"
#include "../atf.h"

#include <fstream>
#include <thread>
#include <future>
#include <netdb.h>
#include <iostream>

//#define USE_CLANG_PREPROCESSOR
#ifdef USE_CLANG_PREPROCESSOR
#include "preprocessor.hpp"
#endif

namespace atf {
namespace cf {
using cuda_device_id = ::std::size_t;

template<typename T = ::std::runtime_error>
auto nvrtc_safe_call(nvrtcResult p_result, const ::std::string &p_message)
-> void {
    if (p_result != NVRTC_SUCCESS) {
        std::cout << p_message << ": " << p_result << std::endl;
        throw T{p_message};
        ::std::exit(EXIT_FAILURE);
    }
}
template<typename T = ::std::runtime_error>
auto cuda_safe_call(cudaError_t p_result, const ::std::string &p_message)
-> void {
    if (p_result != cudaSuccess) {
        std::cout << p_message << ": " << p_result << std::endl;
        throw T{p_message};
        ::std::exit(EXIT_FAILURE);
    }
}

template<typename T = ::std::runtime_error>
auto cuda_safe_call(CUresult p_result, const ::std::string &p_message)
-> void {
    if (p_result != CUDA_SUCCESS) {
        std::cout << p_message << ": " << p_result << std::endl;
        throw T{p_message};
        ::std::exit(EXIT_FAILURE);
    }
}

template<typename GS_0_1, typename GS_1_1, typename GS_2_1,
        typename BS_0_1, typename BS_1_1, typename BS_2_1,
        typename GS_0_2, typename GS_1_2, typename GS_2_2,
        typename BS_0_2, typename BS_1_2, typename BS_2_2,
        typename RES_G_SIZE_CALLABLE,
        typename INT_RES_T,
        typename INT_RES_SIZE_CALLABLE,
        typename NEEDS_SECOND_KERNEL_CALLABLE,
        typename IS_VALID_CALLABLE,
        typename... Ts>
class cuda_md_hom_wrapper_class {
    // helper
    cudaEvent_t _start, _stop;
    unsigned long long best_runtime;
    std::chrono::time_point<std::chrono::system_clock> last_callback;

public:
    cuda_md_hom_wrapper_class(const cuda_device_id &                    device_id,
                              const std::tuple<Ts...>&                  kernel_inputs,
                              const RES_G_SIZE_CALLABLE&                res_g_size,
                              const buffer_class<INT_RES_T>&            int_res_buffer,
                              const INT_RES_SIZE_CALLABLE&              int_res_size,
                              const NEEDS_SECOND_KERNEL_CALLABLE&       needs_second_kernel,
                              const IS_VALID_CALLABLE&                  is_valid,

                              const kernel_info&                        kernel_1,
                              const std::tuple<GS_0_1, GS_1_1, GS_2_1>& grid_size_1,
                              const std::tuple<BS_0_1, BS_1_1, BS_2_1>& block_size_1,
                              const size_t                              num_inputs_1,

                              const kernel_info&                        kernel_2,
                              const std::tuple<GS_0_2, GS_1_2, GS_2_2>& grid_size_2,
                              const std::tuple<BS_0_2, BS_1_2, BS_2_2>& block_size_2,
                              const size_t                              num_inputs_2,

                              const size_t                              num_evaluations,
                              const size_t                              warm_ups,

                              const bool                                silent,
                              const process_wrapper_info&               wrapper,
                              const timeout                             warm_up_timeout,
                              const timeout                             evaluation_timeout,

                              const std::function<void(atf::configuration&, unsigned long long)> &callback,
                              const unsigned long long                              callback_interval) :
            _device_id(device_id),
            _kernel_inputs(kernel_inputs), _res_g_size(res_g_size), _int_res_buffer_data(int_res_buffer), _int_res_size(int_res_size), _needs_second_kernel(needs_second_kernel), _is_valid(is_valid),
            _program_1(), _kernel_source_1(kernel_1.source()), _kernel_name_1(kernel_1.name()), _kernel_flags_1(kernel_1.flags()), _grid_size_pattern_1(grid_size_1), _block_size_pattern_1(block_size_1), _num_inputs_1(num_inputs_1), _kernel_buffers_1(), _kernel_input_sizes_1(),
            _program_2(), _kernel_source_2(kernel_2.source()), _kernel_name_2(kernel_2.name()), _kernel_flags_2(kernel_2.flags()), _grid_size_pattern_2(grid_size_2), _block_size_pattern_2(block_size_2), _num_inputs_2(num_inputs_2), _kernel_buffers_2(), _kernel_input_sizes_2(),
            _num_evaluations(num_evaluations), _warm_ups(warm_ups), _callback(callback), _callback_interval(callback_interval), _wrapper(wrapper), _silent(silent), _gold_ptr(NULL), _warm_up_timeout(warm_up_timeout), _evaluation_timeout(evaluation_timeout),
            _match_id_int_res_buffer(0), _match_weight_int_res_buffer(0), _id_measure_int_res_buffer(0), _match_id_res_g_buffer_1(0), _match_weight_res_g_buffer_1(0), _id_measure_res_g_buffer_1(0), _match_id_res_g_buffer_2(0), _match_weight_res_g_buffer_2(0), _id_measure_res_g_buffer_2(0)
    {
        if (_wrapper.type != NONE && (kernel_1.type() != kernel_info::FILENAME || kernel_2.type() != kernel_info::FILENAME)) {
            std::cerr << "source code has to be submitted as file name when using remote tuning" << std::endl;
        }

        best_runtime = std::numeric_limits<unsigned long long>::max() / _num_evaluations;

        if (kernel_1.type() == kernel_info::FILENAME) {
            if (_wrapper.type == NONE) {
                std::ifstream in_file(kernel_1.source(), std::ifstream::in);
                _kernel_source_1 = std::string((std::istreambuf_iterator<char>(in_file)),
                                               std::istreambuf_iterator<char>());
                in_file.close();
            } else {
                _kernel_source_1 = kernel_1.source();
            }
        }
        if (kernel_2.type() == kernel_info::FILENAME) {
            if (_wrapper.type == NONE) {
                std::ifstream in_file(kernel_2.source(), std::ifstream::in);
                _kernel_source_2 = std::string((std::istreambuf_iterator<char>(in_file)),
                                               std::istreambuf_iterator<char>());
                in_file.close();
            } else {
                _kernel_source_2 = kernel_2.source();
            }
        }

        this->read_input_sizes(_kernel_input_sizes_1,             0, _num_inputs_1, std::make_index_sequence<sizeof...(Ts)>());
        this->read_input_sizes(_kernel_input_sizes_2, _num_inputs_1, _num_inputs_2, std::make_index_sequence<sizeof...(Ts)>());

        switch (wrapper.type) {
            case NONE: {
                init_cuda();
                if (!wrapper.result_file_name.empty() && wrapper.result_file_name != "none") {
                    std::ifstream is(wrapper.result_file_name);
                    std::istream_iterator<INT_RES_T> start(is), end;
                    std::vector<INT_RES_T> expected_result(start, end);
                    INT_RES_T *gold_data = new INT_RES_T[expected_result.size()];
                    for (int i = 0; i < expected_result.size(); ++i) {
                        gold_data[i] = expected_result[i];
                    }
                    check_result(gold_data);
                }
                break;
            }
            case LOCAL: {
                _server_socket = socket(AF_UNIX, SOCK_STREAM, 0);
                if (_server_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = std::tmpnam(nullptr);
                struct sockaddr_un server_addr{};
                memset(&server_addr, 0, sizeof(struct sockaddr_un));
                server_addr.sun_family = AF_UNIX;
                strncpy(server_addr.sun_path, _server.c_str(), sizeof(server_addr.sun_path) - 1);
                if (bind(_server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                if (listen(_server_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
            case REMOTE: {
                _server_socket = socket(AF_INET, SOCK_STREAM, 0);
                if (_server_socket < 0) {
                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in server_addr{};
                memset(&server_addr, 0, sizeof(server_addr));
                server_addr.sin_family = AF_INET;
                server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
                server_addr.sin_port = htons(0);
                if (bind(_server_socket, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) {
                    std::cerr << "error while binding socket: " << strerror(errno) << std::endl;
                    exit(1);
                }
                struct sockaddr_in assigned_server_addr{};
                socklen_t socket_length = sizeof(server_addr);
                if (getsockname(_server_socket, (struct sockaddr *) &assigned_server_addr, &socket_length) < 0) {
                    std::cerr << "error while getting port: " << strerror(errno) << std::endl;
                    exit(1);
                }
                _server = "localhost";
                _port = ntohs(assigned_server_addr.sin_port);
                if (listen(_server_socket, 1) < 0) {
                    std::cerr << "error while setting up queue: " << strerror(errno) << std::endl;
                    exit(1);
                }
                break;
            }
        }

        if (!silent) std::cout << "_warm_ups: " << _warm_ups << std::endl;
        if (!silent) std::cout << "_num_evaluations: " << _num_evaluations << std::endl;

        last_callback = std::chrono::system_clock::now() - std::chrono::seconds(_callback_interval);
    }

    ~cuda_md_hom_wrapper_class() {
        if (_wrapper.type == NONE) {
            if (!_wrapper.result_file_name.empty() && _wrapper.result_file_name != "none") {
                delete[] (INT_RES_T*) _gold_ptr;
            }
            cuda_safe_call<>(cudaEventDestroy(_start), "Failed to destroy start event");
            cuda_safe_call<>(cudaEventDestroy(_stop), "Failed to destroy stop event");
            for (auto &buffer : _kernel_buffers_1) {
                cuda_safe_call<>(cuMemFree(buffer), "Failed to free memory");
            }
            for (auto &buffer : _kernel_buffers_2) {
                cuda_safe_call<>(cuMemFree(buffer), "Failed to free memory");
            }
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_1), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_1), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_1), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_2), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_2), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_2), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_match_id_int_res_buffer), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_match_weight_int_res_buffer), "Failed to free memory");
            cuda_safe_call<>(cuMemFree(_id_measure_int_res_buffer), "Failed to free memory");
            nvrtc_safe_call<>(nvrtcDestroyProgram(&_program_1), "Failed to destroy kernel 1 program");
            nvrtc_safe_call<>(nvrtcDestroyProgram(&_program_2), "Failed to destroy kernel 2 program");
            cuda_safe_call<>(cuCtxDestroy(_context), "Failed to destroy context");
        }
    }

    void init_cuda() {
        // delete existing cuda objects
        _kernel_buffers_2.clear();
        _kernel_buffers_1.clear();
        _kernel_buffer_sizes_1.clear();
        _kernel_buffer_sizes_2.clear();
        _kernel_input_ptrs_1.clear();
        _kernel_input_ptrs_2.clear();

        cuda_safe_call<>(cuInit(0), "Failed to initialize CUDA");
        cuda_safe_call<>(cuDeviceGet(&_device, _device_id), "Failed to retrieve specified device");
        cuda_safe_call<>(cuCtxCreate(&_context, 0, _device), "Failed to create context");

        // Create benchmark events
        cuda_safe_call<>(cudaEventCreate(&_start), "Failed to create start event");
        cuda_safe_call<>(cudaEventCreate(&_stop), "Failed to create stop event");

#ifndef USE_CLANG_PREPROCESSOR
        // create programs
        nvrtc_safe_call<>(
                nvrtcCreateProgram(
                        &_program_1,
                        _kernel_source_1.c_str(),
                        _kernel_name_1.c_str(),
                        0,
                        nullptr,
                        nullptr
                ),
                "Failed to create NVRTC program for first kernel"
        );
        nvrtc_safe_call<>(
                nvrtcCreateProgram(
                        &_program_2,
                        _kernel_source_2.c_str(),
                        _kernel_name_2.c_str(),
                        0,
                        nullptr,
                        nullptr
                ),
                "Failed to create NVRTC program for second kernel"
        );
#endif

        // create kernel input buffers
        _kernel_buffers_1.reserve(2*_num_inputs_1); // reserve enough memory so buffer will not be resized
        _kernel_buffers_2.reserve(2*_num_inputs_2); // reserve enough memory so buffer will not be resized
        this->create_buffers(_kernel_buffers_1, _kernel_input_ptrs_1, _kernel_buffer_sizes_1,             0, _num_inputs_1, std::make_index_sequence<sizeof...(Ts)>());
        this->create_buffers(_kernel_buffers_2, _kernel_input_ptrs_2, _kernel_buffer_sizes_2, _num_inputs_1, _num_inputs_2, std::make_index_sequence<sizeof...(Ts)>());
    }

    void warm_ups(size_t warm_ups) {
        _warm_ups = warm_ups;
    }

    void evaluations(size_t evaluations) {
        _num_evaluations = evaluations;
    }

    unsigned long long operator()(configuration &configuration, std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
                      unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        if (_callback && std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - last_callback).count() >= _callback_interval) {
            _callback(configuration, best_runtime);
            last_callback = std::chrono::system_clock::now();
        }

        // update tp values
        for (auto &tp : configuration) {
            auto tp_value = tp.second;
            tp_value.update_tp();
//                if (!_silent) std::cout << tp.first << "\t" << tp.second.value().size_t_val()<< std::endl;
        }

        if (!_is_valid(configuration)) {
            throw std::exception();
        }

        // calculate timeouts
        unsigned long warm_up_timeout = 0;
        if (_warm_up_timeout.type == ABSOLUTE) {
            warm_up_timeout = _warm_up_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _warm_up_timeout.value.factor) {
            warm_up_timeout = static_cast<unsigned long>(_warm_up_timeout.value.factor * best_runtime);
        }
        unsigned long evaluation_timeout = 0;
        if (_evaluation_timeout.type == ABSOLUTE) {
            evaluation_timeout = _evaluation_timeout.value.absolute;
        } else if (best_runtime != std::numeric_limits<unsigned long long>::max() / _num_evaluations
                   && best_runtime <= std::numeric_limits<unsigned long long>::max() / _evaluation_timeout.value.factor) {
            evaluation_timeout = static_cast<unsigned long>(_evaluation_timeout.value.factor * best_runtime);
        }

        // calculate new int_res size
        size_t new_int_res_size = _int_res_size(configuration);
        if (new_int_res_size == 0) {
            // empty int_res buffer because it is not being used
            // set new_size to 1 to initially create the int_res buffer
            new_int_res_size = 1;
        }
        // calculate new global cache block result buffer size
        size_t new_res_g_size_1 = _res_g_size(1, configuration);
        size_t new_res_g_size_2 = _res_g_size(2, configuration);

        // get grid and block size for first kernel
        size_t gs_0_1 = std::get<0>(_grid_size_pattern_1).get_value();
        size_t gs_1_1 = std::get<1>(_grid_size_pattern_1).get_value();
        size_t gs_2_1 = std::get<2>(_grid_size_pattern_1).get_value();

        size_t bs_0_1 = std::get<0>(_block_size_pattern_1).get_value();
        size_t bs_1_1 = std::get<1>(_block_size_pattern_1).get_value();
        size_t bs_2_1 = std::get<2>(_block_size_pattern_1).get_value();

        // create flags for first kernel
        std::stringstream flags;

        for (const auto &tp : configuration)
            flags << " -D " << tp.second.name() << "=" << tp.second.value();
        // set additional kernel flags for first kernel
        flags << _kernel_flags_1;

        // get grid and block size for second kernel
        size_t gs_0_2 = std::get<0>(_grid_size_pattern_2).get_value();
        size_t gs_1_2 = std::get<1>(_grid_size_pattern_2).get_value();
        size_t gs_2_2 = std::get<2>(_grid_size_pattern_2).get_value();

        size_t bs_0_2 = std::get<0>(_block_size_pattern_2).get_value();
        size_t bs_1_2 = std::get<1>(_block_size_pattern_2).get_value();
        size_t bs_2_2 = std::get<2>(_block_size_pattern_2).get_value();

        // set additional kernel flags for second kernel
        flags << _kernel_flags_2;

        if (runtimes != nullptr) runtimes->resize(2);

        unsigned long long runtime;
        if (_wrapper.type == NONE) {
            runtime = direct_execution(
                    warm_up_timeout, evaluation_timeout, configuration,
                    new_int_res_size, new_res_g_size_1, new_res_g_size_2,
                    gs_0_1, gs_1_1, gs_2_1,
                    bs_0_1, bs_1_1, bs_2_1,
                    gs_0_2, gs_1_2, gs_2_2,
                    bs_0_2, bs_1_2, bs_2_2,
                    flags, runtimes, compile_time, error_code
            );
        } else {
//            runtime = seperate_process_execution(
//                    warm_up_timeout, evaluation_timeout, configuration,
//                    new_int_res_size, new_res_g_size_1, new_res_g_size_2,
//                    gs_0_1, gs_1_1, gs_2_1,
//                    bs_0_1, bs_1_1, bs_2_1,
//                    gs_0_2, gs_1_2, gs_2_2,
//                    bs_0_2, bs_1_2, bs_2_2,
//                    flags, runtimes, compile_time, error_code
//            );
        }
        if (runtime < best_runtime) {
            best_runtime = runtime;
            if (_callback) {
                _callback(configuration, runtime);
                last_callback = std::chrono::system_clock::now();
            }
        }
        return runtime;
    }

    unsigned long long direct_execution(unsigned long warm_up_timeout, unsigned long evaluation_timeout, configuration &configuration,
                            size_t new_int_res_size, size_t new_res_g_size_1, size_t new_res_g_size_2,
                            size_t gs_0_1, size_t gs_1_1, size_t gs_2_1,
                            size_t bs_0_1, size_t bs_1_1, size_t bs_2_1,
                            size_t gs_0_2, size_t gs_1_2, size_t gs_2_2,
                            size_t bs_0_2, size_t bs_1_2, size_t bs_2_2,
                            const std::stringstream &flags, std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
                            unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
        // convert flags to vector of c strings
        std::istringstream iss(flags.str());
        std::vector<std::string> str_flags{std::istream_iterator<std::string>{iss},
                                           std::istream_iterator<std::string>{}};
        ::std::vector<const char *> c_str_flags;
        for (const auto &flag : str_flags) {
            c_str_flags.emplace_back(flag.c_str());
        }

        // adapt size of intermediate results buffer
        static size_t last_int_res_size = 0;
        if (new_int_res_size != last_int_res_size) {
            // replace buffer
            cuda_safe_call<>(cuMemFree(_match_id_int_res_buffer), "Failed to free int res buffer");
            cuda_safe_call<>(cuMemFree(_match_weight_int_res_buffer), "Failed to free int res buffer");
            cuda_safe_call<>(cuMemFree(_id_measure_int_res_buffer), "Failed to free int res buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_id_int_res_buffer, new_int_res_size * sizeof(long)), "Failed to allocate int res buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_weight_int_res_buffer, new_int_res_size * sizeof(double)), "Failed to allocate int res buffer");
            cuda_safe_call<>(cuMemAlloc(&_id_measure_int_res_buffer, new_int_res_size * sizeof(int)), "Failed to allocate int res buffer");
            last_int_res_size = new_int_res_size;
        }

        // adapt size of global cache block results buffer
        if (new_res_g_size_1 > new_int_res_size) {
            // replace buffer
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_id_res_g_buffer_1, new_res_g_size_1 * sizeof(long)), "Failed to allocate res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_weight_res_g_buffer_1, new_res_g_size_1 * sizeof(double)), "Failed to allocate res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_id_measure_res_g_buffer_1, new_res_g_size_1 * sizeof(int)), "Failed to allocate res_g_1 buffer");
        } else {
            // res_g not needed (create with size 1)
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_1), "Failed to free res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_id_res_g_buffer_1, 1), "Failed to allocate res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_weight_res_g_buffer_1, 1), "Failed to allocate res_g_1 buffer");
            cuda_safe_call<>(cuMemAlloc(&_id_measure_res_g_buffer_1, 1), "Failed to allocate res_g_1 buffer");
        }
        if (_needs_second_kernel(configuration) && new_res_g_size_2 > _kernel_buffer_sizes_2.back()) {
            // replace buffer
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_id_res_g_buffer_2, new_res_g_size_2 * sizeof(long)), "Failed to allocate res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_weight_res_g_buffer_2, new_res_g_size_2 * sizeof(double)), "Failed to allocate res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_id_measure_res_g_buffer_2, new_res_g_size_2 * sizeof(int)), "Failed to allocate res_g_2 buffer");
        } else {
            // res_g not needed (create with size 1)
            cuda_safe_call<>(cuMemFree(_match_id_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemFree(_match_weight_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemFree(_id_measure_res_g_buffer_2), "Failed to free res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_id_res_g_buffer_2, 1), "Failed to allocate res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_match_weight_res_g_buffer_2, 1), "Failed to allocate res_g_2 buffer");
            cuda_safe_call<>(cuMemAlloc(&_id_measure_res_g_buffer_2, 1), "Failed to allocate res_g_2 buffer");
        }

        // ------------------------------
        // |          Kernel 1          |
        // ------------------------------

        // compile first kernel
#ifdef USE_CLANG_PREPROCESSOR
        auto start_preprocessing = std::chrono::system_clock::now();
        std::string preprocessed_source;
        preprocessed_source.reserve(_kernel_source_1.length());
        preprocess(_kernel_source_1, preprocessed_source, flags.str());
        nvrtc_safe_call<>(
                nvrtcCreateProgram(
                        &_program_1,
                        preprocessed_source.c_str(),
                        _kernel_name_1.c_str(),
                        0,
                        nullptr,
                        nullptr
                ),
                "Failed to create NVRTC program for first kernel"
        );
        auto end_preprocessing = std::chrono::system_clock::now();
        std::cout << "preprocessing time for first kernel: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_preprocessing - start_preprocessing).count() << "ms" << std::endl;
#endif
        auto t_start = std::chrono::system_clock::now();
        auto t_result = nvrtcCompileProgram(
                _program_1,
                static_cast<int>(c_str_flags.size()),
                c_str_flags.data()
        );
        auto t_end = std::chrono::system_clock::now();
        auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>( t_end - t_start ).count();
        std::cout << "compilation time for first kernel: " << runtime_in_sec << "ms" << std::endl;
        if (compile_time != nullptr) *compile_time += runtime_in_sec;
        if (error_code != nullptr) *error_code = t_result;

        // If compilation failed, retrieve log
        if (t_result != NVRTC_SUCCESS) {
            // Query log size
            size_t t_logSize;
            nvrtc_safe_call<>(nvrtcGetProgramLogSize(_program_1, &t_logSize), "Failed to retrieve log size");

            // Retrieve log
            ::std::vector<char> t_log(t_logSize);
            nvrtc_safe_call<>(nvrtcGetProgramLog(_program_1, t_log.data()), "Failed to retrieve log");

            ::std::cout << flags.str() << std::endl;
            ::std::cout << t_log.data() << ::std::endl;

            throw ::std::exception();
        }

        // Retrieve PTX
        size_t t_ptxSize_1;
        nvrtc_safe_call<>(nvrtcGetPTXSize(_program_1, &t_ptxSize_1), "Failed to retrieve PTX size");

        ::std::vector<char> t_ptxCode_1(t_ptxSize_1);
        nvrtc_safe_call<>(nvrtcGetPTX(_program_1, t_ptxCode_1.data()), "Failed to retrieve PTX code");

        // Load PTX
        CUmodule t_module_1;
        CUfunction t_kernel_1;
        cuda_safe_call<>(cuModuleLoadDataEx(&t_module_1, t_ptxCode_1.data(), 0, nullptr, nullptr), "Failed to load module data");
        cuda_safe_call<>(cuModuleGetFunction(&t_kernel_1, t_module_1, _kernel_name_1.c_str()), "Failed to retrieve kernel handle");

        // add res_g and int_res to input pointers
        std::vector<void *> input_ptrs_1 = _kernel_input_ptrs_1;
        input_ptrs_1.emplace_back(&_match_id_res_g_buffer_1);
        input_ptrs_1.emplace_back(&_match_weight_res_g_buffer_1);
        input_ptrs_1.emplace_back(&_id_measure_res_g_buffer_1);
        input_ptrs_1.emplace_back(&_match_id_int_res_buffer);
        input_ptrs_1.emplace_back(&_match_weight_int_res_buffer);
        input_ptrs_1.emplace_back(&_id_measure_int_res_buffer);

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            CUevent start, stop;
            cuda_safe_call<>(cudaEventCreate(&start), "Failed to create start event");
            cuda_safe_call<>(cudaEventCreate(&stop), "Failed to create stop event");
            cuda_safe_call<>(cudaEventRecord(start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_1,
                    gs_0_1, gs_1_1, gs_2_1,
                    bs_0_1, bs_1_1, bs_2_1,
                    0,
                    nullptr,
                    input_ptrs_1.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(stop), "Failed to record stop event");
            cuda_safe_call<>(wait_for_event(stop, warm_up_timeout), "Failed to synchronize with event");
            cuda_safe_call<>(cudaEventDestroy(start), "Failed to destroy start event");
            cuda_safe_call<>(cudaEventDestroy(stop), "Failed to destroy stop event");
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            cuda_safe_call<>(cudaEventRecord(_start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_1,
                    gs_0_1, gs_1_1, gs_2_1,
                    bs_0_1, bs_1_1, bs_2_1,
                    0,
                    nullptr,
                    input_ptrs_1.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(_stop), "Failed to record stop event");
            cuda_safe_call<>(cudaEventSynchronize(_stop), "Failed to synchronize with event");
        }

        // kernel launch with profiling
        unsigned long long kernel_runtime_in_ns_1 = 0;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            CUevent start, stop;
            cuda_safe_call<>(cudaEventCreate(&start), "Failed to create start event");
            cuda_safe_call<>(cudaEventCreate(&stop), "Failed to create stop event");
            cuda_safe_call<>(cudaEventRecord(start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_1,
                    gs_0_1, gs_1_1, gs_2_1,
                    bs_0_1, bs_1_1, bs_2_1,
                    0,
                    nullptr,
                    input_ptrs_1.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(stop), "Failed to record stop event");
            wait_for_event(stop, evaluation_timeout);

            float runtime = 0;
            cuda_safe_call<>(cudaEventElapsedTime(&runtime, start, stop), "Failed to retrieve elapsed time");
            cuda_safe_call<>(cudaEventDestroy(start), "Failed to destroy start event");
            cuda_safe_call<>(cudaEventDestroy(stop), "Failed to destroy stop event");
            if (runtimes != nullptr) (*runtimes)[0].push_back(static_cast<unsigned long long>(runtime * 1000000));
            kernel_runtime_in_ns_1 += runtime * 1000000;

            if (runtimes == nullptr && _num_evaluations > 1 && kernel_runtime_in_ns_1 > best_runtime * _num_evaluations) {
                if (!_needs_second_kernel(configuration) && _gold_ptr != NULL) check_results_helper(1, configuration);
                return kernel_runtime_in_ns_1;
            }
        }
        for (size_t i = 1; i < _num_evaluations; ++i) {
            cuda_safe_call<>(cudaEventRecord(_start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_1,
                    gs_0_1, gs_1_1, gs_2_1,
                    bs_0_1, bs_1_1, bs_2_1,
                    0,
                    nullptr,
                    input_ptrs_1.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(_stop), "Failed to record stop event");
            cuda_safe_call<>(cudaEventSynchronize(_stop), "Failed to synchronize with event");

            float runtime = 0;
            cuda_safe_call<>(cudaEventElapsedTime(&runtime, _start, _stop), "Failed to retrieve elapsed time");
            if (runtimes != nullptr) (*runtimes)[0].push_back(static_cast<unsigned long long>(runtime * 1000000));
            kernel_runtime_in_ns_1 += runtime * 1000000;

            if (runtimes == nullptr && _num_evaluations > 1 && kernel_runtime_in_ns_1 > best_runtime * _num_evaluations) {
                if (!_needs_second_kernel(configuration) && _gold_ptr != NULL) check_results_helper(1, configuration);
                return kernel_runtime_in_ns_1;
            }
        }
//        INT_RES_T *host_mem = (INT_RES_T *) malloc(new_int_res_size);
//        cuda_safe_call<>(cuMemcpyDtoH(host_mem,
//                                      _int_res_buffer,
//                                      new_int_res_size), "Failed to read device result");
//        std::ofstream f;
//        f.open("md_hom.ppm", std::fstream::out | std::fstream::trunc);
//        f << "P6\n" << 1024 << " " << 1024 << "\n255\n";
//        for (int i = 0; i < 1024; ++i)
//            for (int k = 0; k < 1024; ++k)
//                f << host_mem[i * 1024 + k].r
//                  << host_mem[i * 1024 + k].g
//                  << host_mem[i * 1024 + k].b;
////            {
////                float val = host_mem[i * 2048 + k];
////                unsigned char shade = val / 1000 * 255;
////                rgb_store col = prism_colormap[shade];
////                f << col.red
////                  << col.green
////                  << col.blue;
////            }
//        f.close();
//        free(host_mem);

        kernel_runtime_in_ns_1 /= _num_evaluations;
        if (!_needs_second_kernel(configuration)) {
            // return runtime of first kernel if second one is not needed
            if (_gold_ptr != NULL) check_results_helper(1, configuration);
            return kernel_runtime_in_ns_1;
        }

        // ------------------------------
        // |          Kernel 2          |
        // ------------------------------

        // compile first kernel
#ifdef USE_CLANG_PREPROCESSOR
        start_preprocessing = std::chrono::system_clock::now();
        preprocessed_source;
        preprocessed_source.reserve(_kernel_source_2.length());
        preprocess(_kernel_source_2, preprocessed_source, flags.str());
        nvrtc_safe_call<>(
                nvrtcCreateProgram(
                        &_program_1,
                        preprocessed_source.c_str(),
                        _kernel_name_2.c_str(),
                        0,
                        nullptr,
                        nullptr
                ),
                "Failed to create NVRTC program for second kernel"
        );
        end_preprocessing = std::chrono::system_clock::now();
        std::cout << "preprocessing time for second kernel: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_preprocessing - start_preprocessing).count() << "ms" << std::endl;
#endif
        t_start = std::chrono::system_clock::now();
        t_result = nvrtcCompileProgram(
                _program_2,
                static_cast<int>(c_str_flags.size()),
                c_str_flags.data()
        );
        t_end = std::chrono::system_clock::now();
        runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>( t_end - t_start ).count();
        std::cout << "compilation time for second kernel: " << runtime_in_sec << "ms" << std::endl;
        if (compile_time != nullptr) *compile_time += runtime_in_sec;
        if (error_code != nullptr) *error_code = t_result;

        // If compilation failed, retrieve log
        if (t_result != NVRTC_SUCCESS) {
            // Query log size
            size_t t_logSize;
            nvrtc_safe_call<>(nvrtcGetProgramLogSize(_program_2, &t_logSize), "Failed to retrieve log size");

            // Retrieve log
            ::std::vector<char> t_log(t_logSize);
            nvrtc_safe_call<>(nvrtcGetProgramLog(_program_2, t_log.data()), "Failed to retrieve log");

            ::std::cout << flags.str() << std::endl;
            ::std::cout << t_log.data() << ::std::endl;

            throw ::std::exception();
        }

        // Retrieve PTX
        size_t t_ptxSize_2;
        nvrtc_safe_call<>(nvrtcGetPTXSize(_program_2, &t_ptxSize_2), "Failed to retrieve PTX size");

        ::std::vector<char> t_ptxCode_2(t_ptxSize_2);
        nvrtc_safe_call<>(nvrtcGetPTX(_program_2, t_ptxCode_2.data()), "Failed to retrieve PTX code");

        // Load PTX
        CUmodule t_module_2;
        CUfunction t_kernel_2;
        cuda_safe_call<>(cuModuleLoadDataEx(&t_module_2, t_ptxCode_2.data(), 0, 0, 0), "Failed to load module data");
        cuda_safe_call<>(cuModuleGetFunction(&t_kernel_2, t_module_2, _kernel_name_2.c_str()), "Failed to retrieve kernel handle");

        // add res_g and int_res to input pointers
        std::vector<void *> input_ptrs_2;
        input_ptrs_2.emplace_back(&_match_id_int_res_buffer);
        input_ptrs_2.emplace_back(&_match_weight_int_res_buffer);
        input_ptrs_2.emplace_back(&_id_measure_int_res_buffer);
        input_ptrs_2.emplace_back(&_match_id_res_g_buffer_2);
        input_ptrs_2.emplace_back(&_match_weight_res_g_buffer_2);
        input_ptrs_2.emplace_back(&_id_measure_res_g_buffer_2);
        for (const auto &ptr : _kernel_input_ptrs_2)
            input_ptrs_2.emplace_back(ptr);

        // warm ups
        if (_warm_ups > 0) {
            // run first warm up with timeout
            CUevent start, stop;
            cuda_safe_call<>(cudaEventCreate(&start), "Failed to create start event");
            cuda_safe_call<>(cudaEventCreate(&stop), "Failed to create stop event");
            cuda_safe_call<>(cudaEventRecord(start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_2,
                    gs_0_2, gs_2_2, gs_2_2,
                    bs_0_2, bs_1_2, bs_2_2,
                    0,
                    nullptr,
                    input_ptrs_2.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(stop), "Failed to record stop event");
            wait_for_event(stop, warm_up_timeout);
            cuda_safe_call<>(cudaEventDestroy(start), "Failed to destroy start event");
            cuda_safe_call<>(cudaEventDestroy(stop), "Failed to destroy stop event");
        }
        for (size_t i = 1; i < _warm_ups; ++i) {
            cuda_safe_call<>(cudaEventRecord(_start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_2,
                    gs_0_2, gs_1_2, gs_2_2,
                    bs_0_2, bs_1_2, bs_2_2,
                    0,
                    nullptr,
                    input_ptrs_2.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(_stop), "Failed to record stop event");
            cuda_safe_call<>(cudaEventSynchronize(_stop), "Failed to synchronize with event");
        }

        // kernel launch with profiling
        unsigned long long kernel_runtime_in_ns_2 = 0;
        if (_num_evaluations > 0) {
            // run first evaluation with timeout
            CUevent start, stop;
            cuda_safe_call<>(cudaEventCreate(&start), "Failed to create start event");
            cuda_safe_call<>(cudaEventCreate(&stop), "Failed to create stop event");
            cuda_safe_call<>(cudaEventRecord(start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_2,
                    gs_0_2, gs_1_2, gs_2_2,
                    bs_0_2, bs_1_2, bs_2_2,
                    0,
                    nullptr,
                    input_ptrs_2.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(stop), "Failed to record stop event");
            wait_for_event(stop, evaluation_timeout);

            float runtime = 0;
            cuda_safe_call<>(cudaEventElapsedTime(&runtime, start, stop), "Failed to retrieve elapsed time");
            cuda_safe_call<>(cudaEventDestroy(start), "Failed to destroy start event");
            cuda_safe_call<>(cudaEventDestroy(stop), "Failed to destroy stop event");
            if (runtimes != nullptr) (*runtimes)[0].push_back(static_cast<unsigned long long>(runtime * 1000000));
            kernel_runtime_in_ns_2 += runtime * 1000000;

            if (runtimes == nullptr && _num_evaluations > 1 && kernel_runtime_in_ns_2 > (best_runtime - kernel_runtime_in_ns_1) * _num_evaluations) {
                if (_gold_ptr != NULL) check_results_helper(2, configuration);
                return kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2;
            }
        }
        for (size_t i = 1; i < _num_evaluations; ++i) {
            cuda_safe_call<>(cudaEventRecord(_start), "Failed to record start event");
            cuda_safe_call<>(cuLaunchKernel(
                    t_kernel_2,
                    gs_0_2, gs_1_2, gs_2_2,
                    bs_0_2, bs_1_2, bs_2_2,
                    0,
                    nullptr,
                    input_ptrs_2.data(),
                    nullptr
            ), "Failed to launch kernel");
            cuda_safe_call<>(cudaEventRecord(_stop), "Failed to record stop event");
            cuda_safe_call<>(cudaEventSynchronize(_stop), "Failed to synchronize with event");

            float runtime = 0;
            cuda_safe_call<>(cudaEventElapsedTime(&runtime, _start, _stop), "Failed to retrieve elapsed time");
            if (runtimes != nullptr) (*runtimes)[0].push_back(static_cast<unsigned long long>(runtime * 1000000));
            kernel_runtime_in_ns_2 += runtime * 1000000;

            if (runtimes == nullptr && _num_evaluations > 1 && kernel_runtime_in_ns_2 > (best_runtime - kernel_runtime_in_ns_1) * _num_evaluations) {
                if (_gold_ptr != NULL) check_results_helper(2, configuration);
                return kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2 / (i + 1);
            }
        }
        kernel_runtime_in_ns_2 /= _num_evaluations;
        if (_gold_ptr != NULL) check_results_helper(2, configuration);
        return kernel_runtime_in_ns_1 + kernel_runtime_in_ns_2;
    }

//    unsigned long long seperate_process_execution(unsigned long warm_up_timeout, unsigned long evaluation_timeout, configuration &configuration,
//                                      size_t new_int_res_size, size_t new_res_g_size_1, size_t new_res_g_size_2,
//                                      size_t gs_0_1, size_t gs_1_1, size_t gs_2_1,
//                                      size_t bs_0_1, size_t bs_1_1, size_t bs_2_1,
//                                      size_t gs_0_2, size_t gs_1_2, size_t gs_2_2,
//                                      size_t bs_0_2, size_t bs_1_2, size_t bs_2_2,
//                                      const std::stringstream &flags, std::vector<std::vector<unsigned long long>> *runtimes = nullptr,
//                                      unsigned long long *compile_time = nullptr, int *error_code = nullptr) {
//        addr client_addr{};
//        int client_socket = -1;
//        socklen_t client_addr_length = _wrapper.type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in);
//        auto wait_for_connection = [&] () {
//            struct sockaddr* addr_ptr = nullptr;
//            if (_wrapper.type == LOCAL) {
//                addr_ptr = (struct sockaddr*) &client_addr.addr_un;
//            } else {
//                addr_ptr = (struct sockaddr*) &client_addr.addr_in;
//            }
//            client_socket = accept(_server_socket,  addr_ptr, &client_addr_length);
//            if (client_socket < 0) {
//                std::cerr << "error while accepting client connection: " << strerror(errno) << std::endl;
//                exit(1);
//            }
//        };
//        std::thread wait_for_connection_thread(wait_for_connection);
//
//        std::string command = _wrapper.command_prefix;
//        command.append(" --device-id ").append(std::to_string(_device_id));
//        command.append(" --routine-name ")
//                .append(_kernel_name_1).append(" ")
//                .append(_kernel_name_2);
//        command.append(" --source-file \"")
//                .append(_kernel_source_1).append("\" \"")
//                .append(_kernel_source_2).append("\"");
//        command.append(" --flags \"").append(flags.str()).append("\"");
//
//        command.append(" --kernel-1-input-sizes");
//        make_process_wrapper_input_sizes_flags(command, 0, _num_inputs_1, std::make_index_sequence<sizeof...(Ts)>());
//        command.append(" --kernel-1-res-g-size ").append(std::to_string(new_res_g_size_1 / sizeof(INT_RES_T)));
//        command.append(" --kernel-1-result-size ").append(std::to_string(new_int_res_size / sizeof(INT_RES_T)));
//        command.append(" --kernel-1-grid-size ")
//                .append(std::to_string(gs_0_1)).append(" ")
//                .append(std::to_string(gs_1_1)).append(" ")
//                .append(std::to_string(gs_2_1));
//        command.append(" --kernel-1-block-size ")
//                .append(std::to_string(bs_0_1)).append(" ")
//                .append(std::to_string(bs_1_1)).append(" ")
//                .append(std::to_string(bs_2_1));
//
//        command.append(" --needs-kernel-2 ").append(_needs_second_kernel(configuration) ? "true" : "false");
//        command.append(" --kernel-2-res-g-size ").append(std::to_string(new_res_g_size_2 / sizeof(INT_RES_T)));
//        command.append(" --kernel-2-input-sizes");
//        make_process_wrapper_input_sizes_flags(command, _num_inputs_1, _num_inputs_2, std::make_index_sequence<sizeof...(Ts)>());
//        command.append(" --kernel-2-grid-size ")
//                .append(std::to_string(gs_0_2)).append(" ")
//                .append(std::to_string(gs_1_2)).append(" ")
//                .append(std::to_string(gs_2_2));
//        command.append(" --kernel-2-block-size ")
//                .append(std::to_string(bs_0_2)).append(" ")
//                .append(std::to_string(bs_1_2)).append(" ")
//                .append(std::to_string(bs_2_2));
//
//        if (_wrapper.type == LOCAL) {
//            command.append(" --server-type local");
//            command.append(" --server ").append("\"").append(_server).append("\"");
//            command.append(" --port 0");
//        } else {
//            command.append(" --server-type remote");
//            command.append(" --server ").append("\"").append(_server).append("\"");
//            command.append(" --port ").append(std::to_string(_port));
//        }
//        command.append(" --warm-ups ").append(std::to_string(_warm_ups));
//        command.append(" --evaluations ").append(std::to_string(_num_evaluations));
//        command.append(" --warm-up-timeout ").append(std::to_string(warm_up_timeout));
//        command.append(" --evaluation-timeout ").append(std::to_string(evaluation_timeout));
//        command.append(" --expected-result-file ").append("\"").append(_wrapper.result_file_name).append("\"");
//        command.append(" --benchmark ");
//        if (runtimes != nullptr) {
//            command.append("true");
//        } else {
//            command.append("false");
//        }
//        command.append(" --buffer-fill-algorithm \"").append(_wrapper.buffer_fill_algorithm).append("\"");
//
//        // execute process wrapper
//        int ret_val;
//        std::atomic_bool cancel(false);
//        auto exec_command = [&] () {
//            ret_val = system(command.c_str());
//
//            if (client_socket < 0) {
//                // process wrapper could not connect before failing
//                // cancel waiting for connection by connecting to server socket locally
//                cancel = true;
//                int cancel_client_socket = socket(_wrapper.type == LOCAL ? AF_UNIX : AF_INET, SOCK_STREAM, 0);
//                if (cancel_client_socket < 0) {
//                    std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
//                    exit(1);
//                }
//
//                atf::cf::addr server_addr{};
//                if (_wrapper.type == LOCAL) {
//                    server_addr.addr_un.sun_family = AF_UNIX;
//                    strncpy(server_addr.addr_un.sun_path, _server.c_str(), sizeof(server_addr.addr_un.sun_path) - 1);
//                } else {
//                    memset(&server_addr.addr_in, 0, sizeof(server_addr.addr_in));
//                    server_addr.addr_in.sin_family = AF_INET;
//                    server_addr.addr_in.sin_port = htons(_port);
//                    struct hostent *hostinfo;
//                    hostinfo = gethostbyname(_server.c_str());
//                    if (hostinfo == nullptr)
//                    {
//                        std::cerr << "invalid server: " << _server << std::endl;
//                        exit(1);
//                    }
//                    server_addr.addr_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;
//                }
//
//                struct sockaddr* addr_ptr = nullptr;
//                if (_wrapper.type == LOCAL) {
//                    addr_ptr = (struct sockaddr*) &server_addr.addr_un;
//                } else {
//                    addr_ptr = (struct sockaddr*) &server_addr.addr_in;
//                }
//                if (connect(cancel_client_socket, addr_ptr, _wrapper.type == LOCAL ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in)) < 0) {
//                    std::cerr << "error while connecting to server: " << strerror(errno) << std::endl;
//                    exit(1);
//                }
//                if (close(cancel_client_socket) != 0) {
//                    std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")" << std::endl;
//                    exit(1);
//                }
//            }
//        };
//        std::thread exec_command_thread(exec_command);
//
//        // wait for client connection
//        wait_for_connection_thread.join();
//
//        // if process wrapper failed, throw exception
//        if (cancel) {
//            exec_command_thread.join();
//            // close client connection
//            if (close(client_socket) != 0) {
//                std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")" << std::endl;
//                exit(1);
//            }
//            throw std::exception();
//        }
//
//        // determine what values to receive
//        uint16_t mask_buf = 0;
//        auto mask_data = (char *) (&mask_buf);
//        size_t left = sizeof(mask_buf);
//        ssize_t rc;
//        do {
//            rc = recv(client_socket, mask_data, left, 0);
//            if (rc < 0) {
//                std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")" << std::endl;
//                exit(1);
//            }
//            mask_data += rc;
//            left -= rc;
//        } while (left > 0);
//        short recv_mask = htons(mask_buf);
//        auto recv_compile_time = static_cast<bool>(recv_mask & 1);
//        auto recv_error_code = static_cast<bool>(recv_mask & 2);
//        auto recv_runtimes = static_cast<bool>(recv_mask & 4);
//
//        if (recv_compile_time) {
//            // read compile time
//            __uint64_t buf;
//            auto data = (char *) (&buf);
//            left = sizeof(buf);
//            do {
//                rc = recv(client_socket, data, left, 0);
//                if (rc < 0) {
//                    std::cerr << "error while receiving compile time: " << strerror(errno) << "(" << errno << ")"
//                              << std::endl;
//                    exit(1);
//                }
//                data += rc;
//                left -= rc;
//            } while (left > 0);
//            if (compile_time != nullptr) *compile_time = be64toh(buf);
//        }
//
//        if (recv_error_code) {
//            // read error code
//            uint32_t buf;
//            auto data = (char *) (&buf);
//            left = sizeof(buf);
//            do {
//                rc = recv(client_socket, data, left, 0);
//                if (rc < 0) {
//                    std::cerr << "error while receiving error code: " << strerror(errno) << "(" << errno << ")"
//                              << std::endl;
//                    exit(1);
//                }
//                data += rc;
//                left -= rc;
//            } while (left > 0);
//            if (error_code != nullptr) {
//                *error_code = ntohl(buf);
//            }
//            data = (char *) (&buf);
//            left = sizeof(buf);
//            do {
//                rc = recv(client_socket, data, left, 0);
//                if (rc < 0) {
//                    std::cerr << "error while receiving sign of error code: " << strerror(errno) << "(" << errno << ")"
//                              << std::endl;
//                    exit(1);
//                }
//                data += rc;
//                left -= rc;
//            } while (left > 0);
//            if (error_code != nullptr) {
//                if (ntohl(buf) != 0) {
//                    *error_code *= -1;
//                }
//            }
//        }
//
//        unsigned long long runtime = 0;
//        if (recv_runtimes) {
//            // read client runtime
//            __uint64_t buf;
//            auto data = (char *) (&buf);
//            size_t left = sizeof(buf);
//            ssize_t rc;
//            do {
//                rc = recv(client_socket, data, left, 0);
//                if (rc < 0) {
//                    std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")"
//                              << std::endl;
//                    exit(1);
//                }
//                data += rc;
//                left -= rc;
//            } while (left > 0);
//            runtime = be64toh(buf);
//
//            if (runtimes != nullptr) {
//                // when benchmarking, receive all runtimes
//                std::vector<size_t> kernel_ids = {0};
//                if (_needs_second_kernel(configuration)) {
//                    kernel_ids.push_back(1);
//                }
//                for (size_t id : kernel_ids) {
//                    for (int i = 0; i < _num_evaluations; ++i) {
//                        data = (char *) (&buf);
//                        left = sizeof(buf);
//                        do {
//                            rc = recv(client_socket, data, left, 0);
//                            if (rc < 0) {
//                                std::cerr << "error while receiving runtime: " << strerror(errno) << "(" << errno << ")"
//                                          << std::endl;
//                                exit(1);
//                            }
//                            data += rc;
//                            left -= rc;
//                        } while (left > 0);
//                        (*runtimes)[id].push_back(be64toh(buf));
//                    }
//                }
//            }
//        }
//
//        // close client connection
//        if (close(client_socket) != 0) {
//            std::cerr << "error while closing client socket: " << strerror(errno) << "(" << errno << ")" << std::endl;
//            exit(1);
//        }
//
//
//        // wait for process wrapper to terminate
//        exec_command_thread.join();
//
//        if (!recv_runtimes) throw std::exception();
//        return runtime;
//    }

    void check_result(void* gold_ptr) {
        _gold_ptr = gold_ptr;
    }

private:
    const IS_VALID_CALLABLE&            _is_valid;

    const cuda_device_id                _device_id;
          CUdevice                      _device;
          CUcontext                     _context;

    std::tuple<Ts...>                   _kernel_inputs;
    CUdeviceptr                         _match_id_res_g_buffer_1;
    CUdeviceptr                         _match_weight_res_g_buffer_1;
    CUdeviceptr                         _id_measure_res_g_buffer_1;
    CUdeviceptr                         _match_id_res_g_buffer_2;
    CUdeviceptr                         _match_weight_res_g_buffer_2;
    CUdeviceptr                         _id_measure_res_g_buffer_2;
    const RES_G_SIZE_CALLABLE&          _res_g_size;
    const buffer_class<INT_RES_T>&      _int_res_buffer_data;
    CUdeviceptr                          _match_id_int_res_buffer;
    CUdeviceptr                          _match_weight_int_res_buffer;
    CUdeviceptr                          _id_measure_int_res_buffer;
    const INT_RES_SIZE_CALLABLE&        _int_res_size;
    const NEEDS_SECOND_KERNEL_CALLABLE& _needs_second_kernel;

    // Kernel 1
    nvrtcProgram                        _program_1;
    std::string                         _kernel_source_1;
    std::string                         _kernel_name_1;
    std::string                         _kernel_flags_1;
    std::tuple<GS_0_1, GS_1_1, GS_2_1>  _grid_size_pattern_1;
    std::tuple<BS_0_1, BS_1_1, BS_2_1>  _block_size_pattern_1;
    size_t                              _num_inputs_1;
    std::vector<CUdeviceptr>            _kernel_buffers_1;
    std::vector<void *>                 _kernel_input_ptrs_1;
    std::vector<size_t>                 _kernel_buffer_sizes_1;
    std::vector<size_t>                 _kernel_input_sizes_1;

    // Kernel 2
    nvrtcProgram                        _program_2;
    std::string                         _kernel_source_2;
    std::string                         _kernel_name_2;
    std::string                         _kernel_flags_2;
    std::tuple<GS_0_2, GS_1_2, GS_2_2>  _grid_size_pattern_2;
    std::tuple<BS_0_2, BS_1_2, BS_2_2>  _block_size_pattern_2;
    size_t                              _num_inputs_2;
    std::vector<CUdeviceptr>            _kernel_buffers_2;
    std::vector<void *>                 _kernel_input_ptrs_2;
    std::vector<size_t>                 _kernel_buffer_sizes_2;
    std::vector<size_t>                 _kernel_input_sizes_2;

    size_t                              _num_evaluations;
    size_t                              _warm_ups;

    const std::function<void(atf::configuration&, unsigned long long)> _callback;
    const unsigned long long                                           _callback_interval;

    const process_wrapper_info          _wrapper;
    int                                 _server_socket;
    std::string                         _server;
    unsigned short int                  _port;

    const bool                          _silent;
    const timeout                       _warm_up_timeout;
    const timeout                       _evaluation_timeout;

    void*                               _gold_ptr;

//    // helper for creating arguments for process wrapper
//    template<size_t... Is>
//    void make_process_wrapper_input_sizes_flags(std::string &flags,
//                                                size_t offset, size_t count,
//                                                std::index_sequence<Is...>) {
//        make_process_wrapper_input_sizes_flags_impl(flags,
//                                                    offset, count,
//                                                    std::get<Is>(_kernel_inputs)...);
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     scalar<float> &scalar, ARGs &... args) {
//        if (offset == 0) {
//            std::stringstream ss;
//            ss << std::fixed << std::setprecision(std::numeric_limits<float>::max_digits10) << scalar.get();
//            flags.append(" ").append("sf ").append(ss.str());
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     scalar<double> &scalar, ARGs &... args) {
//        if (offset == 0) {
//            std::stringstream ss;
//            ss << std::fixed << std::setprecision(std::numeric_limits<double>::max_digits10) << scalar.get();
//            flags.append(" ").append("sd ").append(ss.str());
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     scalar<size_t> &scalar, ARGs &... args) {
//        if (offset == 0) {
//            flags.append(" ").append("sst ").append(std::to_string(scalar.get()));
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     buffer_class<float> &buffer, ARGs &... args) {
//        if (offset == 0) {
//            flags.append(" ").append("wbf ").append(std::to_string(buffer.size()));
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     buffer_class<double> &buffer, ARGs &... args) {
//        if (offset == 0) {
//            flags.append(" ").append("wbd ").append(std::to_string(buffer.size()));
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     buffer_class<size_t> &buffer, ARGs &... args) {
//        if (offset == 0) {
//            flags.append(" ").append("wbst ").append(std::to_string(buffer.size()));
//
//            if (count > 1) {
//                make_process_wrapper_input_sizes_flags_impl(flags,
//                                                            offset, count - 1,
//                                                            args...);
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    template<typename... ARGs>
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags,
//                                                     size_t offset, size_t count,
//                                                     const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
//        if (offset == 0) {
//            for (const auto &runtime_input : runtime_inputs) {
//                size_t elem_count;
//                switch (runtime_input._type) {
//                    case LINEAR_BUFFER_FLOAT:
//                        flags.append(" ").append("lbf ").append(runtime_input._value);
//                        break;
//                    case LINEAR_BUFFER_DOUBLE:
//                        flags.append(" ").append("lbd ").append(runtime_input._value);
//                        break;
//                    case LINEAR_BUFFER_SIZE_T:
//                        flags.append(" ").append("lbst ").append(runtime_input._value);
//                        break;
//                    case WRAP_BUFFER_FLOAT:
//                        flags.append(" ").append("wbf ").append(runtime_input._value);
//                        break;
//                    case WRAP_BUFFER_DOUBLE:
//                        flags.append(" ").append("wbd ").append(runtime_input._value);
//                        break;
//                    case WRAP_BUFFER_SIZE_T:
//                        flags.append(" ").append("wbst ").append(runtime_input._value);
//                        break;
//                    case SCALAR_FLOAT:
//                        flags.append(" ").append("sf ").append(runtime_input._value);
//                        break;
//                    case SCALAR_DOUBLE:
//                        flags.append(" ").append("sd ").append(runtime_input._value);
//                        break;
//                    case SCALAR_SIZE_T:
//                        flags.append(" ").append("sst ").append(runtime_input._value);
//                        break;
//                }
//            }
//        } else {
//            make_process_wrapper_input_sizes_flags_impl(flags,
//                                                        offset - 1, count,
//                                                        args...);
//        }
//    }
//    void make_process_wrapper_input_sizes_flags_impl(std::string &flags, size_t offset, size_t count) {}

    // helper for reading input sizes
    template< size_t... Is>
    void read_input_sizes(std::vector<size_t>& _kernel_input_sizes,
                        size_t offset, size_t count,
                        std::index_sequence<Is...>) {
        read_input_sizes_impl(_kernel_input_sizes,
                              offset, count,
                              std::get<Is>(_kernel_inputs)...);
    }

    template<typename T, typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t>& _kernel_input_sizes,
                               size_t offset, size_t count,
                               const scalar<T> &scalar, ARGs&... args) {
        if (offset == 0) {
            _kernel_input_sizes.emplace_back(1);

            if (count > 1) {
                read_input_sizes_impl(_kernel_input_sizes,
                                      offset, count - 1,
                                      args...);
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    template<typename T, typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t>& _kernel_input_sizes,
                               size_t offset, size_t count,
                               const buffer_class<T> &buffer, ARGs&... args) {
        if (offset == 0) {
            // add buffer size to _kernel_input_sizes
            _kernel_input_sizes.emplace_back(buffer.size());

            if (count > 1) {
                read_input_sizes_impl(_kernel_input_sizes,
                                      offset, count - 1,
                                      args...);
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    template<typename... ARGs>
    void read_input_sizes_impl(std::vector<size_t> &_kernel_input_sizes,
                               size_t offset, size_t count,
                               const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        // add buffer size to _kernel_input_sizes
                        size_t input_size;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> input_size)) {
                            std::cerr << "unable to parse buffer size " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        _kernel_input_sizes.emplace_back(input_size);
                        break;
                    }
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // add scalar size (1) to _kernel_input_sizes
                        _kernel_input_sizes.emplace_back(1);
                        break;
                }
            }
        } else {
            read_input_sizes_impl(_kernel_input_sizes,
                                  offset - 1, count,
                                  args...);
        }
    }

    void read_input_sizes_impl(std::vector<size_t>& _kernel_input_sizes,
                               size_t offset, size_t count) {}

    // helper for creating buffers
    template< size_t... Is>
    void create_buffers(std::vector<CUdeviceptr>& _kernel_buffers,
                        std::vector<void *>& _kernel_input_ptrs,
                        std::vector<size_t>& _kernel_buffer_sizes,
                        size_t offset, size_t count,
                        std::index_sequence<Is...>) {
        create_buffers_impl(_kernel_buffers,
                            _kernel_input_ptrs,
                            _kernel_buffer_sizes,
                            offset, count,
                            std::get<Is>(_kernel_inputs)...);
    }

    template<typename T, typename... ARGs>
    void create_buffers_impl(std::vector<CUdeviceptr>& _kernel_buffers,
                             std::vector<void *>& _kernel_input_ptrs,
                             std::vector<size_t>& _kernel_buffer_sizes,
                             size_t offset, size_t count,
                             scalar<T> &scalar, ARGs&... args) {
        if (offset == 0) {
            _kernel_input_ptrs.emplace_back(scalar.get_ptr());

            if (count > 1) {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_input_ptrs,
                                    _kernel_buffer_sizes,
                                    offset, count - 1,
                                    args...);
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_input_ptrs,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    template<typename T, typename... ARGs>
    void create_buffers_impl(std::vector<CUdeviceptr>& _kernel_buffers,
                             std::vector<void *>& _kernel_input_ptrs,
                             std::vector<size_t>& _kernel_buffer_sizes,
                             size_t offset, size_t count,
                             const buffer_class<T> &buffer, ARGs&... args) {
        if (offset == 0 && buffer.size() > 0) {
            auto start_time = std::chrono::system_clock::now();

            // add buffer size to _kernel_input_sizes
            _kernel_buffer_sizes.emplace_back(buffer.size() * sizeof(T));

            // create buffer
            _kernel_buffers.emplace_back();
            auto &ptr = _kernel_buffers.back();
            cuda_safe_call<>(cuMemAlloc(&ptr, buffer.size() * sizeof(T)), "Failed to allocate buffer");
            cuda_safe_call<>(cuMemcpyHtoD(ptr, buffer.get(), buffer.size() * sizeof(T)), "Failed to copy buffer data to device");
            cuda_safe_call<>(cudaDeviceSynchronize(), "Failed to synchronize");
            _kernel_input_ptrs.emplace_back(&ptr);

            auto end_time = std::chrono::system_clock::now();
            auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
            if (!_silent) std::cout << "Time to create and fill buffer: " << runtime << "ms" << std::endl;

            if (count > 1) {
                create_buffers_impl(_kernel_buffers,
                                    _kernel_input_ptrs,
                                    _kernel_buffer_sizes,
                                    offset, count - 1,
                                    args...);
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_input_ptrs,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    template<typename... ARGs>
    void create_buffers_impl(std::vector<CUdeviceptr>& _kernel_buffers,
                             std::vector<void *>& _kernel_input_ptrs,
                             std::vector<size_t>& _kernel_buffer_sizes,
                             size_t offset, size_t count,
                             const std::vector<runtime_input> &runtime_inputs, ARGs &... args) {
        if (offset == 0) {
            for (const auto &runtime_input : runtime_inputs) {
                size_t elem_count;
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        // get element count
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> elem_count)) {
                            std::cerr << "unable to parse element count " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        break;
                    }
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        // get element count
                        elem_count = 1;
                        break;
                }

                // get element size
                size_t elem_size;
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case WRAP_BUFFER_FLOAT:
                    case SCALAR_FLOAT:
                        elem_size = sizeof(float);
                        break;
                    case LINEAR_BUFFER_DOUBLE:
                    case WRAP_BUFFER_DOUBLE:
                    case SCALAR_DOUBLE:
                        elem_size = sizeof(double);
                        break;
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_SIZE_T:
                    case SCALAR_SIZE_T:
                        elem_size = sizeof(size_t);
                        break;
                }

                auto start_time = std::chrono::system_clock::now();

                void *data = malloc(elem_count * elem_size);
                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                        for (int i = 0; i < elem_count; ++i) ((float *) data)[i] = i;
                        break;
                    case WRAP_BUFFER_FLOAT:
                        for (int i = 0; i < elem_count; ++i) ((float *) data)[i] = (i % 10) + 1;
                        break;
                    case LINEAR_BUFFER_DOUBLE:
                        for (int i = 0; i < elem_count; ++i) ((double *) data)[i] = i;
                        break;
                    case WRAP_BUFFER_DOUBLE:
                        for (int i = 0; i < elem_count; ++i) ((double *) data)[i] = (i % 10) + 1;
                        break;
                    case LINEAR_BUFFER_SIZE_T:
                        for (int i = 0; i < elem_count; ++i) ((size_t *) data)[i] = static_cast<size_t>(i);
                        break;
                    case WRAP_BUFFER_SIZE_T:
                        for (int i = 0; i < elem_count; ++i) ((size_t *) data)[i] = static_cast<size_t>((i % 10) + 1);
                        break;
                    case SCALAR_FLOAT: {
                        float val;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> val)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        memcpy(data, &val, elem_size);
                        break;
                    }
                    case SCALAR_DOUBLE: {
                        double val;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> val)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        memcpy(data, &val, elem_size);
                        break;
                    }
                    case SCALAR_SIZE_T: {
                        size_t val;
                        std::stringstream ss(runtime_input._value);
                        if (!(ss >> val)) {
                            std::cerr << "unable to parse scalar value " << runtime_input._value << std::endl;
                            exit(EXIT_FAILURE);
                        }
                        memcpy(data, &val, elem_size);
                        break;
                    }
                }


                switch (runtime_input._type) {
                    case LINEAR_BUFFER_FLOAT:
                    case LINEAR_BUFFER_DOUBLE:
                    case LINEAR_BUFFER_SIZE_T:
                    case WRAP_BUFFER_FLOAT:
                    case WRAP_BUFFER_DOUBLE:
                    case WRAP_BUFFER_SIZE_T: {
                        // add buffer size to _kernel_input_sizes
                        _kernel_buffer_sizes.emplace_back(elem_count * elem_size);

                        // create buffer
                        _kernel_buffers.emplace_back();
                        auto &ptr = _kernel_buffers.back();
                        cuda_safe_call<>(cuMemAlloc(&ptr, elem_count * elem_size), "Failed to allocate buffer");
                        cuda_safe_call<>(cuMemcpyHtoD(ptr, data, elem_count * elem_size),
                                         "Failed to copy buffer data to device");
                        cuda_safe_call<>(cudaDeviceSynchronize(), "Failed to synchronize");
                        _kernel_input_ptrs.emplace_back(&ptr);

                        free(data);
                        break;
                    }
                    case SCALAR_FLOAT:
                    case SCALAR_DOUBLE:
                    case SCALAR_SIZE_T:
                        _kernel_input_ptrs.emplace_back(data);
                        // TODO memory leak: data is not freed (irrelevant for now, because only one tuning per program instance will be started)
                        break;
                }

                auto end_time = std::chrono::system_clock::now();
                auto runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();
                if (!_silent) std::cout << "Time to create and fill buffer: " << runtime << "ms" << std::endl;
            }
        } else {
            create_buffers_impl(_kernel_buffers,
                                _kernel_input_ptrs,
                                _kernel_buffer_sizes,
                                offset - 1, count,
                                args...);
        }
    }

    void create_buffers_impl(std::vector<CUdeviceptr>& _kernel_buffers,
                             std::vector<void *>& _kernel_input_ptrs,
                             std::vector<size_t>& _kernel_buffer_sizes,
                             size_t offset, size_t count) {}


    void check_results_helper(int kernel, configuration &config) {
//        size_t result_buffer_size;
//        if (!_kernel_buffer_sizes_2.empty()) {
//            result_buffer_size = _kernel_buffer_sizes_2.back();
//        } else {
//            result_buffer_size = _int_res_size(config);
//        }
//        void *dev_result = malloc(result_buffer_size);
//        cuda_safe_call<>(cuMemcpyDtoH(dev_result,
//                                      kernel == 1 ? _int_res_buffer : _kernel_buffers_2.back(),
//                                      result_buffer_size), "Failed to read device result");
//        if (memcmp(dev_result, _gold_ptr, result_buffer_size)) {
//            std::ofstream log_file;
//            log_file.open("errors.txt", std::ofstream::out | std::ofstream::app);
//            log_file << "!!! ERROR IN RESULT !!!" << std::endl;
//            log_file << "configuration: " << std::endl;
//            for (const auto &val : config) {
//                log_file << "#define " << val.first << " " << val.second.value() << std::endl;
//            }
//            log_file << "result buffer: " << std::endl;
//            for (int k = 0; k < result_buffer_size / sizeof(INT_RES_T); ++k) {
//                log_file << ((INT_RES_T *) dev_result)[k] << "\t";
//            }
//            log_file << std::endl << "gold buffer: " << std::endl;
//            for (int k = 0; k < result_buffer_size / sizeof(INT_RES_T); ++k) {
//                log_file << ((INT_RES_T *) _gold_ptr)[k] << "\t";
//            }
//            log_file << std::endl;
//            log_file.close();
////                throw std::exception();
//            std::cout << "result is incorrect" << std::endl;
//            exit(1);
//        } else  {
//            std::cout << "result is correct" << std::endl;
//        }
//        free(dev_result);
        assert(false && "not implemented");
    }

    cudaError_t wait_for_event(const cudaEvent_t &event, unsigned long timeout = 0) {
        if (timeout > 0) {
            auto timeout_lambda = [this, event, timeout]() {
                ns_sleep(timeout);

                auto result = cudaEventQuery(event);
                if (result != cudaErrorInvalidResourceHandle && result != cudaSuccess) {
                    // kernel still running
                    std::cerr << "kernel took more than " << timeout << " ns, killing process" << std::endl;
                    exit(1);
                }
            };
            std::thread timeout_thread(timeout_lambda);

            auto error = cudaEventSynchronize(event);
            timeout_thread.detach();
            return error;
        } else {
            return cudaEventSynchronize(event);
        }
    }

};



template<typename GS_0_1, typename GS_1_1, typename GS_2_1,
        typename BS_0_1, typename BS_1_1, typename BS_2_1,
        typename GS_0_2, typename GS_1_2, typename GS_2_2,
        typename BS_0_2, typename BS_1_2, typename BS_2_2,
        typename RES_G_SIZE_CALLABLE,
        typename INT_RES_T,
        typename INT_RES_SIZE_CALLABLE,
        typename NEEDS_SECOND_KERNEL_CALLABLE,
        typename IS_VALID_CALLABLE,
        typename... Ts_1,
        typename... Ts_2>
auto cuda_md_hom(const cuda_device_id &                   device_id,

                const kernel_info&                        kernel_1,
                const std::tuple<Ts_1...>&                kernel_inputs_1,
                const std::tuple<GS_0_1, GS_1_1, GS_2_1>& grid_size_1,
                const std::tuple<BS_0_1, BS_1_1, BS_2_1>& block_size_1,

                const kernel_info&                        kernel_2,
                const std::tuple<Ts_2...>&                kernel_inputs_2,
                const std::tuple<GS_0_2, GS_1_2, GS_2_2>& grid_size_2,
                const std::tuple<BS_0_2, BS_1_2, BS_2_2>& block_size_2,

                const RES_G_SIZE_CALLABLE&                res_g_size,
                const buffer_class<INT_RES_T>&            int_res_buffer,
                const INT_RES_SIZE_CALLABLE&              int_res_size,
                const NEEDS_SECOND_KERNEL_CALLABLE&       needs_second_kernel,
                const IS_VALID_CALLABLE&                  is_valid,

                const size_t                              num_evaluations,
                const size_t                              warm_ups,

                const bool                                silent = false,
                const process_wrapper_info&               wrapper = {NONE, "", "", ""},
                const timeout                             warm_up_timeout = {ABSOLUTE, {.absolute = 0}},
                const timeout                             evaluation_timeout = {ABSOLUTE, {.absolute = 0}},

                const std::function<void(atf::configuration&, unsigned long long)> &callback = {},
                const unsigned long long                              callback_interval = 100) {
    return cuda_md_hom_wrapper_class<GS_0_1, GS_1_1, GS_2_1, BS_0_1, BS_1_1, BS_2_1,
            GS_0_2, GS_1_2, GS_2_2, BS_0_2, BS_1_2, BS_2_2,
            RES_G_SIZE_CALLABLE, INT_RES_T, INT_RES_SIZE_CALLABLE, NEEDS_SECOND_KERNEL_CALLABLE, IS_VALID_CALLABLE,
            Ts_1..., Ts_2...>
            (device_id, std::tuple_cat(kernel_inputs_1, kernel_inputs_2),
             res_g_size, int_res_buffer, int_res_size, needs_second_kernel, is_valid,
             kernel_1, grid_size_1, block_size_1, sizeof...(Ts_1),
             kernel_2, grid_size_2, block_size_2, sizeof...(Ts_2),
             num_evaluations, warm_ups, silent, wrapper, warm_up_timeout, evaluation_timeout, callback, callback_interval);
}
}
}

#endif //MD_HOM_CUDA_MD_HOM_WRAPPER_HPP
