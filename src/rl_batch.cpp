#define READ_DATA

typedef struct {
    char str[46];
} str46;
typedef struct {
    char values[2];
} chr2;
typedef struct {
    double values[8];
} dbl8;

#include <iostream>
#include <CL/cl.hpp>
#include <fstream>
#include <map>
#include <sstream>
#include <chrono>
#include <cassert>
#include <unordered_map>
#include <iomanip>
#include <ctpl_stl.h>
#include "../libraries/argparse.hpp"


void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("Error with errorcode: %d\n", err);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, const char *argv[]) {
    constexpr int BATCH_SIZE_NEW_REPORTS = 1024;
    constexpr int BATCH_SIZE_EXISTING_REPORTS = 32768;

    ArgumentParser args;
    args.appName("rl_batch");
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id", 1, false);
    args.addArgument("--num-threads", 1, false);
    args.addArgument("--num-new-reports", 1, false);
    args.addArgument("--num-existing-reports", 1, false);
    args.addArgument("--num-wg-n", 1, false);
    args.addArgument("--num-wg-e", 1, false);
    args.addArgument("--num-wi-n", 1, false);
    args.addArgument("--num-wi-e", 1, false);
    args.addArgument("--cb-size-n", 1, false);
    args.addArgument("--cb-size-e", 1, false);
    args.parse(static_cast<size_t>(argc), argv);
    const int NUM_NEW_REPORTS = args.retrieve_int("num-new-reports");
    const int NUM_EXISTING_REPORTS = args.retrieve_int("num-existing-reports");
    const int PLATFORM_ID = args.retrieve_int("platform-id");
    const int DEVICE_ID = args.retrieve_int("device-id");
    const int NUM_THREADS = args.retrieve_int("num-threads");
    const int NUM_WG_L_1 = args.retrieve_int("num-wg-n");
    const int NUM_WG_R_1 = args.retrieve_int("num-wg-e");
    const int NUM_WI_L_1 = args.retrieve_int("num-wi-n");
    const int NUM_WI_R_1 = args.retrieve_int("num-wi-e");
    const int CB_SIZE_L_1 = args.retrieve_int("cb-size-n");
    const int CB_SIZE_R_1 = args.retrieve_int("cb-size-e");

    // configuration
    std::map<std::string, int> configuration;
    configuration["G_CB_SIZE_L_1"] = BATCH_SIZE_NEW_REPORTS;
    configuration["G_CB_SIZE_R_1"] = BATCH_SIZE_EXISTING_REPORTS;
    configuration["CACHE_L_CB"] = 0;
    configuration["CACHE_P_CB"] = 0;
    configuration["G_CB_RES_DEST_LEVEL"] = 2;
    configuration["L_CB_RES_DEST_LEVEL"] = 1;
    configuration["P_CB_RES_DEST_LEVEL"] = 0;
    configuration["OCL_DIM_L_1"] = 1;
    configuration["OCL_DIM_R_1"] = 0;
    configuration["P_CB_SIZE_L_1"] = 1;
    configuration["P_CB_SIZE_R_1"] = 1;

    configuration["NUM_WG_L_1"] = NUM_WG_L_1;
    configuration["NUM_WI_L_1"] = NUM_WI_L_1;
    configuration["NUM_WG_R_1"] = NUM_WG_R_1;
    configuration["NUM_WI_R_1"] = NUM_WI_R_1;

    configuration["L_CB_SIZE_L_1"] = CB_SIZE_L_1 != 0 ? CB_SIZE_L_1 : (BATCH_SIZE_NEW_REPORTS + configuration["NUM_WG_L_1"] - 1) / configuration["NUM_WG_L_1"];
    configuration["L_CB_SIZE_R_1"] = CB_SIZE_R_1 != 0 ? CB_SIZE_R_1 : (BATCH_SIZE_EXISTING_REPORTS + configuration["NUM_WG_R_1"] - 1) / configuration["NUM_WG_R_1"];

    const dbl8 probM =
            {{
                     0.99439781, // lastname
                     0.98247962, // firstname
                     0.98772576, // birthname
                     0.99949391, // birthday
                     0.9995371,  // birthmonth
                     0.99971322, // birthyear
                     0.99999304, // gender
                     0.9915867   // cid
             }};

    std::vector<long>      n_id(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname3(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname3(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group1(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group2(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group3(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname3(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthday(NUM_NEW_REPORTS);
    std::vector<chr2>      n_gender(NUM_NEW_REPORTS);
    std::vector<int>       n_birthmonth(NUM_NEW_REPORTS);
    std::vector<int>       n_birthyear(NUM_NEW_REPORTS);
    std::vector<int>       n_cin(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthday(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_gender(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthmonth(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthyear(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_cin(NUM_NEW_REPORTS);

#ifdef READ_DATA
    std::string n_file_name = "data/n_" + std::to_string(NUM_NEW_REPORTS) + ".csv";
//    std::cout << "loading new reports from " << n_file_name << std::endl;
    std::unordered_map<std::string, long> firstname_groups;
    std::ifstream n_csv_file(n_file_name);
    int li = 0;
    std::string line;
    while (std::getline(n_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> n_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(n_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> n_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(n_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> n_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(n_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> n_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(n_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> n_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(n_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> n_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(n_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> n_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(n_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> n_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(n_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> n_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(n_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> n_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(n_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> n_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> n_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> n_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> n_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> n_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(n_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> n_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> n_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> n_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (n_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname1[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group1[li] = firstname_groups[std::string(n_firstname1[li].str, 46)];
        }
        if (n_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname2[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group2[li] = firstname_groups[std::string(n_firstname2[li].str, 46)];
        }
        if (n_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname3[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group3[li] = firstname_groups[std::string(n_firstname3[li].str, 46)];
        }
        n_prob_birthyear[li] = 1.0E-7;
        n_prob_gender[li] = 1.0E-7;
        n_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == NUM_NEW_REPORTS);
    n_csv_file.close();

#endif

    std::vector<long>      i_id(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname3(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group1(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group2(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthday(NUM_EXISTING_REPORTS);
    std::vector<chr2>      i_gender(NUM_EXISTING_REPORTS);
    std::vector<int>       i_birthmonth(NUM_EXISTING_REPORTS);
    std::vector<int>       i_birthyear(NUM_EXISTING_REPORTS);
    std::vector<int>       i_cin(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthday(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_gender(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthmonth(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthyear(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_cin(NUM_EXISTING_REPORTS);

#ifdef READ_DATA
    std::string i_file_name = "data/i_" + std::to_string(NUM_EXISTING_REPORTS) + ".csv";
//    std::cout << "loading inventory reports from " << i_file_name << std::endl;
    std::ifstream i_csv_file(i_file_name);
    li = 0;
    while (std::getline(i_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> i_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(i_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> i_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(i_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> i_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(i_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> i_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(i_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> i_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(i_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> i_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(i_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> i_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(i_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> i_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(i_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> i_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(i_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> i_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(i_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> i_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> i_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> i_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> i_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> i_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(i_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> i_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> i_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> i_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (i_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname1[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group1[li] = firstname_groups[std::string(i_firstname1[li].str, 46)];
        }
        if (i_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname2[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group2[li] = firstname_groups[std::string(i_firstname2[li].str, 46)];
        }
        if (i_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname3[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group3[li] = firstname_groups[std::string(i_firstname3[li].str, 46)];
        }
        i_prob_birthyear[li] = 1.0E-7;
        i_prob_gender[li] = 1.0E-7;
        i_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == NUM_EXISTING_REPORTS);
    i_csv_file.close();
#endif

    std::vector<long> res_match_id(NUM_NEW_REPORTS);
    std::vector<double> res_match_weight(NUM_NEW_REPORTS);
    std::vector<int> res_id_measure(NUM_NEW_REPORTS);

    // read kernel code
    std::string kernel_source_1;
    std::ifstream file_1("rl_1.cl");
    kernel_source_1 = std::string((std::istreambuf_iterator<char>(file_1)),
                                  std::istreambuf_iterator<char>());
    file_1.close();
    std::string kernel_source_2;
    std::ifstream file_2("rl_2.cl");
    kernel_source_2 = std::string((std::istreambuf_iterator<char>(file_2)),
                                  std::istreambuf_iterator<char>());
    file_2.close();

    // get platform
    cl_int error;
    std::vector<cl::Platform> platforms;
    error = cl::Platform::get(&platforms); check_error(error);
    if (PLATFORM_ID >= platforms.size()) exit(EXIT_FAILURE);
    cl::Platform _platform = platforms[PLATFORM_ID];
    std::string platform_name, platform_vendor;
    error = _platform.getInfo(CL_PLATFORM_NAME, &platform_name); check_error(error);
    error = _platform.getInfo(CL_PLATFORM_VENDOR, &platform_vendor); check_error(error);
//    std::cout << "Using platform \"" << platform_name << "\" of vendor \"" << platform_vendor << "\"" << std::endl;

    // get device
    std::vector<cl::Device> devices;
    error = _platform.getDevices(CL_DEVICE_TYPE_ALL, &devices); check_error(error);
    if (DEVICE_ID >= devices.size()) {
        std::cout << "device with id " << DEVICE_ID << " not found" << std::endl;
        exit(EXIT_FAILURE);
    }
    cl::Device _device = devices[DEVICE_ID];
    std::string device_name;
    error = _device.getInfo(CL_DEVICE_NAME, &device_name); check_error(error);
//    std::cout << "Using device \"" << device_name << "\"" << std::endl;

    // create context and command queue
    cl_context_properties props[] = {CL_CONTEXT_PLATFORM,
                                     reinterpret_cast<cl_context_properties>( _platform()),
                                     0
    };
    cl::Context _context(VECTOR_CLASS<cl::Device>(1, _device), props);
    cl::CommandQueue _command_queue(_context, _device, CL_QUEUE_PROFILING_ENABLE);

    std::string kernel_name_1 = "rl_1";
    std::string kernel_name_2 = "rl_2";

    // declare buffer
    cl::Buffer n_id_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer n_lastname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_lastname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_lastname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_firstname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_firstname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_firstname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_firstname_group1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer n_firstname_group2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer n_firstname_group3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer n_birthname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_birthname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_birthname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_birthday_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer n_gender_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(chr2), nullptr, &error); check_error(error);
    cl::Buffer n_birthmonth_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer n_birthyear_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer n_cin_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer n_prob_lastname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_lastname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_lastname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_firstname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_firstname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_firstname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthday_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_gender_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthmonth_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_birthyear_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer n_prob_cin_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_NEW_REPORTS * sizeof(double), nullptr, &error); check_error(error);

    cl::Buffer i_id_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer i_lastname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_lastname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_lastname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_firstname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_firstname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_firstname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_firstname_group1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer i_firstname_group2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer i_firstname_group3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer i_birthname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_birthname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_birthname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_birthday_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(str46), nullptr, &error); check_error(error);
    cl::Buffer i_gender_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(chr2), nullptr, &error); check_error(error);
    cl::Buffer i_birthmonth_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer i_birthyear_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer i_cin_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(int), nullptr, &error); check_error(error);
    cl::Buffer i_prob_lastname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_lastname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_lastname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_firstname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_firstname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_firstname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthname1_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthname2_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthname3_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthday_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_gender_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthmonth_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_birthyear_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer i_prob_cin_buf(_context, CL_MEM_READ_ONLY, BATCH_SIZE_EXISTING_REPORTS * sizeof(double), nullptr, &error); check_error(error);

    size_t int_res_size = BATCH_SIZE_NEW_REPORTS * configuration["NUM_WG_R_1"];
    cl::Buffer match_id_int_res_buf(_context, CL_MEM_READ_WRITE, int_res_size * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer match_weight_int_res_buf(_context, CL_MEM_READ_WRITE, int_res_size * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer id_measure_int_res_buf(_context, CL_MEM_READ_WRITE, int_res_size * sizeof(int), nullptr, &error); check_error(error);
    size_t res_g_size = BATCH_SIZE_NEW_REPORTS;
    if (configuration["G_CB_RES_DEST_LEVEL"] == 2) res_g_size *= configuration["NUM_WG_R_1"];
    if (configuration["L_CB_RES_DEST_LEVEL"] == 2) res_g_size *= configuration["NUM_WI_R_1"];
    if (res_g_size == int_res_size) res_g_size = 1;
    cl::Buffer match_id_res_g_buf(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(long), nullptr, &error); check_error(error);
    cl::Buffer match_weight_res_g_buf(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(double), nullptr, &error); check_error(error);
    cl::Buffer id_measure_res_g_buf(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(int), nullptr, &error); check_error(error);

    cl::Buffer match_id_res_buf;
    cl::Buffer match_weight_res_buf;
    cl::Buffer id_measure_res_buf;
    auto ceil_lambda = [](auto x, auto y){ return (x + y - 1) / y; };
    if (configuration["NUM_WG_R_1"] > 1) {
        size_t res_size = BATCH_SIZE_EXISTING_REPORTS;
        match_id_res_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_size * sizeof(long), nullptr, &error); check_error(error);
        match_weight_res_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_size * sizeof(double), nullptr, &error); check_error(error);
        id_measure_res_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_size * sizeof(int), nullptr, &error); check_error(error);
        res_g_size = BATCH_SIZE_EXISTING_REPORTS;
        if (configuration["L_CB_RES_DEST_LEVEL"] == 2) res_g_size *= std::min(configuration["NUM_WI_R_1"], ceil_lambda(std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))), std::min(configuration["P_CB_SIZE_R_1"], std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))))));
        if (res_g_size == res_size) res_g_size = 1;
        match_id_res_g_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(long), nullptr, &error); check_error(error);
        match_weight_res_g_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(double), nullptr, &error); check_error(error);
        id_measure_res_g_buf = cl::Buffer(_context, CL_MEM_READ_WRITE, res_g_size * sizeof(int), nullptr, &error); check_error(error);
    }

    // build first kernel
    cl::Program _program_1(_context,
                           cl::Program::Sources(1, std::make_pair(kernel_source_1.c_str(),
                                                                  kernel_source_1.length()))
    );
    std::stringstream flags;
    for (const auto tp : configuration) {
        flags << " -D" << tp.first << "=" << tp.second;
    }
    auto chrono_start = std::chrono::system_clock::now();
    error = _program_1.build(std::vector<cl::Device>(1, _device), flags.str().c_str());
    if (error != CL_SUCCESS) {
        std::cout << flags.str() << std::endl;
        std::cout << error << std::endl;
        if (error == CL_BUILD_PROGRAM_FAILURE) {
            auto buildLog = _program_1.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
            std::cout << std::endl << "Build of first kernel failed! Log:" << std::endl << buildLog << std::endl;
        }
        exit(EXIT_FAILURE);
    }
    auto chrono_end = std::chrono::system_clock::now();
    auto runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//    std::cout << "compilation time for first kernel: " << runtime_in_sec << "ms" << std::endl;
    auto buildLog = _program_1.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
//    std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
    auto kernel_1 = cl::Kernel(_program_1, kernel_name_1.c_str(), &error); check_error(error);
    unsigned int i = 0;
    kernel_1.setArg(i++, probM);
    kernel_1.setArg(i++, n_id_buf);
    kernel_1.setArg(i++, n_lastname1_buf);
    kernel_1.setArg(i++, n_lastname2_buf);
    kernel_1.setArg(i++, n_lastname3_buf);
    kernel_1.setArg(i++, n_firstname1_buf);
    kernel_1.setArg(i++, n_firstname2_buf);
    kernel_1.setArg(i++, n_firstname3_buf);
    kernel_1.setArg(i++, n_firstname_group1_buf);
    kernel_1.setArg(i++, n_firstname_group2_buf);
    kernel_1.setArg(i++, n_firstname_group3_buf);
    kernel_1.setArg(i++, n_birthname1_buf);
    kernel_1.setArg(i++, n_birthname2_buf);
    kernel_1.setArg(i++, n_birthname3_buf);
    kernel_1.setArg(i++, n_birthday_buf);
    kernel_1.setArg(i++, n_gender_buf);
    kernel_1.setArg(i++, n_birthmonth_buf);
    kernel_1.setArg(i++, n_birthyear_buf);
    kernel_1.setArg(i++, n_cin_buf);
    kernel_1.setArg(i++, n_prob_lastname1_buf);
    kernel_1.setArg(i++, n_prob_lastname2_buf);
    kernel_1.setArg(i++, n_prob_lastname3_buf);
    kernel_1.setArg(i++, n_prob_firstname1_buf);
    kernel_1.setArg(i++, n_prob_firstname2_buf);
    kernel_1.setArg(i++, n_prob_firstname3_buf);
    kernel_1.setArg(i++, n_prob_birthname1_buf);
    kernel_1.setArg(i++, n_prob_birthname2_buf);
    kernel_1.setArg(i++, n_prob_birthname3_buf);
    kernel_1.setArg(i++, n_prob_birthday_buf);
    kernel_1.setArg(i++, n_prob_gender_buf);
    kernel_1.setArg(i++, n_prob_birthmonth_buf);
    kernel_1.setArg(i++, n_prob_birthyear_buf);
    kernel_1.setArg(i++, n_prob_cin_buf);
    kernel_1.setArg(i++, i_id_buf);
    kernel_1.setArg(i++, i_lastname1_buf);
    kernel_1.setArg(i++, i_lastname2_buf);
    kernel_1.setArg(i++, i_lastname3_buf);
    kernel_1.setArg(i++, i_firstname1_buf);
    kernel_1.setArg(i++, i_firstname2_buf);
    kernel_1.setArg(i++, i_firstname3_buf);
    kernel_1.setArg(i++, i_firstname_group1_buf);
    kernel_1.setArg(i++, i_firstname_group2_buf);
    kernel_1.setArg(i++, i_firstname_group3_buf);
    kernel_1.setArg(i++, i_birthname1_buf);
    kernel_1.setArg(i++, i_birthname2_buf);
    kernel_1.setArg(i++, i_birthname3_buf);
    kernel_1.setArg(i++, i_birthday_buf);
    kernel_1.setArg(i++, i_gender_buf);
    kernel_1.setArg(i++, i_birthmonth_buf);
    kernel_1.setArg(i++, i_birthyear_buf);
    kernel_1.setArg(i++, i_cin_buf);
    kernel_1.setArg(i++, i_prob_lastname1_buf);
    kernel_1.setArg(i++, i_prob_lastname2_buf);
    kernel_1.setArg(i++, i_prob_lastname3_buf);
    kernel_1.setArg(i++, i_prob_firstname1_buf);
    kernel_1.setArg(i++, i_prob_firstname2_buf);
    kernel_1.setArg(i++, i_prob_firstname3_buf);
    kernel_1.setArg(i++, i_prob_birthname1_buf);
    kernel_1.setArg(i++, i_prob_birthname2_buf);
    kernel_1.setArg(i++, i_prob_birthname3_buf);
    kernel_1.setArg(i++, i_prob_birthday_buf);
    kernel_1.setArg(i++, i_prob_gender_buf);
    kernel_1.setArg(i++, i_prob_birthmonth_buf);
    kernel_1.setArg(i++, i_prob_birthyear_buf);
    kernel_1.setArg(i++, i_prob_cin_buf);
    kernel_1.setArg(i++, match_id_res_g_buf);
    kernel_1.setArg(i++, match_weight_res_g_buf);
    kernel_1.setArg(i++, id_measure_res_g_buf);
    kernel_1.setArg(i++, match_id_int_res_buf);
    kernel_1.setArg(i++, match_weight_int_res_buf);
    kernel_1.setArg(i++, id_measure_int_res_buf);

    std::stringstream flags_l;
    cl::Kernel kernel_1_l;
    if (NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS != 0) {
        // build first kernel for last batch in l-dim
        cl::Program _program_1_l(_context,
                                 cl::Program::Sources(1, std::make_pair(kernel_source_1.c_str(),
                                                                        kernel_source_1.length()))
        );
        auto configuration_l = configuration;
        configuration_l["G_CB_SIZE_L_1"] = NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS;
        configuration_l["L_CB_SIZE_L_1"] = ((NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS) + configuration["NUM_WG_L_1"] - 1) / configuration["NUM_WG_L_1"];
        for (const auto tp : configuration_l) {
            flags_l << " -D" << tp.first << "=" << tp.second;
        }
        chrono_start = std::chrono::system_clock::now();
        error = _program_1_l.build(std::vector<cl::Device>(1, _device), flags_l.str().c_str());
        if (error != CL_SUCCESS) {
            std::cout << flags_l.str() << std::endl;
            std::cout << error << std::endl;
            if (error == CL_BUILD_PROGRAM_FAILURE) {
                auto buildLog = _program_1_l.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
                check_error(error);
                std::cout << std::endl << "Build of first kernel for last batch in l-dim failed! Log:" << std::endl << buildLog << std::endl;
            }
            exit(EXIT_FAILURE);
        }
        chrono_end = std::chrono::system_clock::now();
        runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//        std::cout << "compilation time for first kernel for last batch in l-dim: " << runtime_in_sec << "ms" << std::endl;
        buildLog = _program_1_l.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
        check_error(error);
//        std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
        kernel_1_l = cl::Kernel(_program_1_l, kernel_name_1.c_str(), &error);
        check_error(error);
        i = 0;
        kernel_1_l.setArg(i++, probM);
        kernel_1_l.setArg(i++, n_id_buf);
        kernel_1_l.setArg(i++, n_lastname1_buf);
        kernel_1_l.setArg(i++, n_lastname2_buf);
        kernel_1_l.setArg(i++, n_lastname3_buf);
        kernel_1_l.setArg(i++, n_firstname1_buf);
        kernel_1_l.setArg(i++, n_firstname2_buf);
        kernel_1_l.setArg(i++, n_firstname3_buf);
        kernel_1_l.setArg(i++, n_firstname_group1_buf);
        kernel_1_l.setArg(i++, n_firstname_group2_buf);
        kernel_1_l.setArg(i++, n_firstname_group3_buf);
        kernel_1_l.setArg(i++, n_birthname1_buf);
        kernel_1_l.setArg(i++, n_birthname2_buf);
        kernel_1_l.setArg(i++, n_birthname3_buf);
        kernel_1_l.setArg(i++, n_birthday_buf);
        kernel_1_l.setArg(i++, n_gender_buf);
        kernel_1_l.setArg(i++, n_birthmonth_buf);
        kernel_1_l.setArg(i++, n_birthyear_buf);
        kernel_1_l.setArg(i++, n_cin_buf);
        kernel_1_l.setArg(i++, n_prob_lastname1_buf);
        kernel_1_l.setArg(i++, n_prob_lastname2_buf);
        kernel_1_l.setArg(i++, n_prob_lastname3_buf);
        kernel_1_l.setArg(i++, n_prob_firstname1_buf);
        kernel_1_l.setArg(i++, n_prob_firstname2_buf);
        kernel_1_l.setArg(i++, n_prob_firstname3_buf);
        kernel_1_l.setArg(i++, n_prob_birthname1_buf);
        kernel_1_l.setArg(i++, n_prob_birthname2_buf);
        kernel_1_l.setArg(i++, n_prob_birthname3_buf);
        kernel_1_l.setArg(i++, n_prob_birthday_buf);
        kernel_1_l.setArg(i++, n_prob_gender_buf);
        kernel_1_l.setArg(i++, n_prob_birthmonth_buf);
        kernel_1_l.setArg(i++, n_prob_birthyear_buf);
        kernel_1_l.setArg(i++, n_prob_cin_buf);
        kernel_1_l.setArg(i++, i_id_buf);
        kernel_1_l.setArg(i++, i_lastname1_buf);
        kernel_1_l.setArg(i++, i_lastname2_buf);
        kernel_1_l.setArg(i++, i_lastname3_buf);
        kernel_1_l.setArg(i++, i_firstname1_buf);
        kernel_1_l.setArg(i++, i_firstname2_buf);
        kernel_1_l.setArg(i++, i_firstname3_buf);
        kernel_1_l.setArg(i++, i_firstname_group1_buf);
        kernel_1_l.setArg(i++, i_firstname_group2_buf);
        kernel_1_l.setArg(i++, i_firstname_group3_buf);
        kernel_1_l.setArg(i++, i_birthname1_buf);
        kernel_1_l.setArg(i++, i_birthname2_buf);
        kernel_1_l.setArg(i++, i_birthname3_buf);
        kernel_1_l.setArg(i++, i_birthday_buf);
        kernel_1_l.setArg(i++, i_gender_buf);
        kernel_1_l.setArg(i++, i_birthmonth_buf);
        kernel_1_l.setArg(i++, i_birthyear_buf);
        kernel_1_l.setArg(i++, i_cin_buf);
        kernel_1_l.setArg(i++, i_prob_lastname1_buf);
        kernel_1_l.setArg(i++, i_prob_lastname2_buf);
        kernel_1_l.setArg(i++, i_prob_lastname3_buf);
        kernel_1_l.setArg(i++, i_prob_firstname1_buf);
        kernel_1_l.setArg(i++, i_prob_firstname2_buf);
        kernel_1_l.setArg(i++, i_prob_firstname3_buf);
        kernel_1_l.setArg(i++, i_prob_birthname1_buf);
        kernel_1_l.setArg(i++, i_prob_birthname2_buf);
        kernel_1_l.setArg(i++, i_prob_birthname3_buf);
        kernel_1_l.setArg(i++, i_prob_birthday_buf);
        kernel_1_l.setArg(i++, i_prob_gender_buf);
        kernel_1_l.setArg(i++, i_prob_birthmonth_buf);
        kernel_1_l.setArg(i++, i_prob_birthyear_buf);
        kernel_1_l.setArg(i++, i_prob_cin_buf);
        kernel_1_l.setArg(i++, match_id_res_g_buf);
        kernel_1_l.setArg(i++, match_weight_res_g_buf);
        kernel_1_l.setArg(i++, id_measure_res_g_buf);
        kernel_1_l.setArg(i++, match_id_int_res_buf);
        kernel_1_l.setArg(i++, match_weight_int_res_buf);
        kernel_1_l.setArg(i++, id_measure_int_res_buf);
    }

    cl::Kernel kernel_1_r;
    if (NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS != 0) {
        // build first kernel for last batch in r-dim
        cl::Program _program_1_r(_context,
                                 cl::Program::Sources(1, std::make_pair(kernel_source_1.c_str(),
                                                                        kernel_source_1.length()))
        );
        std::stringstream flags_r;
        auto configuration_r = configuration;
        configuration_r["G_CB_SIZE_R_1"] = NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS;
        configuration_r["L_CB_SIZE_R_1"] = ((NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS) + configuration["NUM_WG_R_1"] - 1) / configuration["NUM_WG_R_1"];
        for (const auto tp : configuration_r) {
            flags_r << " -D" << tp.first << "=" << tp.second;
        }
        chrono_start = std::chrono::system_clock::now();
        error = _program_1_r.build(std::vector<cl::Device>(1, _device), flags_r.str().c_str());
        if (error != CL_SUCCESS) {
            std::cout << flags_r.str() << std::endl;
            std::cout << error << std::endl;
            if (error == CL_BUILD_PROGRAM_FAILURE) {
                auto buildLog = _program_1_r.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
                check_error(error);
                std::cout << std::endl << "Build of first kernel for last batch in r-dim failed! Log:" << std::endl << buildLog << std::endl;
            }
            exit(EXIT_FAILURE);
        }
        chrono_end = std::chrono::system_clock::now();
        runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//        std::cout << "compilation time for first kernel for last batch in r-dim: " << runtime_in_sec << "ms" << std::endl;
        buildLog = _program_1_r.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
        check_error(error);
//        std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
        kernel_1_r = cl::Kernel(_program_1_r, kernel_name_1.c_str(), &error);
        check_error(error);
        i = 0;
        kernel_1_r.setArg(i++, probM);
        kernel_1_r.setArg(i++, n_id_buf);
        kernel_1_r.setArg(i++, n_lastname1_buf);
        kernel_1_r.setArg(i++, n_lastname2_buf);
        kernel_1_r.setArg(i++, n_lastname3_buf);
        kernel_1_r.setArg(i++, n_firstname1_buf);
        kernel_1_r.setArg(i++, n_firstname2_buf);
        kernel_1_r.setArg(i++, n_firstname3_buf);
        kernel_1_r.setArg(i++, n_firstname_group1_buf);
        kernel_1_r.setArg(i++, n_firstname_group2_buf);
        kernel_1_r.setArg(i++, n_firstname_group3_buf);
        kernel_1_r.setArg(i++, n_birthname1_buf);
        kernel_1_r.setArg(i++, n_birthname2_buf);
        kernel_1_r.setArg(i++, n_birthname3_buf);
        kernel_1_r.setArg(i++, n_birthday_buf);
        kernel_1_r.setArg(i++, n_gender_buf);
        kernel_1_r.setArg(i++, n_birthmonth_buf);
        kernel_1_r.setArg(i++, n_birthyear_buf);
        kernel_1_r.setArg(i++, n_cin_buf);
        kernel_1_r.setArg(i++, n_prob_lastname1_buf);
        kernel_1_r.setArg(i++, n_prob_lastname2_buf);
        kernel_1_r.setArg(i++, n_prob_lastname3_buf);
        kernel_1_r.setArg(i++, n_prob_firstname1_buf);
        kernel_1_r.setArg(i++, n_prob_firstname2_buf);
        kernel_1_r.setArg(i++, n_prob_firstname3_buf);
        kernel_1_r.setArg(i++, n_prob_birthname1_buf);
        kernel_1_r.setArg(i++, n_prob_birthname2_buf);
        kernel_1_r.setArg(i++, n_prob_birthname3_buf);
        kernel_1_r.setArg(i++, n_prob_birthday_buf);
        kernel_1_r.setArg(i++, n_prob_gender_buf);
        kernel_1_r.setArg(i++, n_prob_birthmonth_buf);
        kernel_1_r.setArg(i++, n_prob_birthyear_buf);
        kernel_1_r.setArg(i++, n_prob_cin_buf);
        kernel_1_r.setArg(i++, i_id_buf);
        kernel_1_r.setArg(i++, i_lastname1_buf);
        kernel_1_r.setArg(i++, i_lastname2_buf);
        kernel_1_r.setArg(i++, i_lastname3_buf);
        kernel_1_r.setArg(i++, i_firstname1_buf);
        kernel_1_r.setArg(i++, i_firstname2_buf);
        kernel_1_r.setArg(i++, i_firstname3_buf);
        kernel_1_r.setArg(i++, i_firstname_group1_buf);
        kernel_1_r.setArg(i++, i_firstname_group2_buf);
        kernel_1_r.setArg(i++, i_firstname_group3_buf);
        kernel_1_r.setArg(i++, i_birthname1_buf);
        kernel_1_r.setArg(i++, i_birthname2_buf);
        kernel_1_r.setArg(i++, i_birthname3_buf);
        kernel_1_r.setArg(i++, i_birthday_buf);
        kernel_1_r.setArg(i++, i_gender_buf);
        kernel_1_r.setArg(i++, i_birthmonth_buf);
        kernel_1_r.setArg(i++, i_birthyear_buf);
        kernel_1_r.setArg(i++, i_cin_buf);
        kernel_1_r.setArg(i++, i_prob_lastname1_buf);
        kernel_1_r.setArg(i++, i_prob_lastname2_buf);
        kernel_1_r.setArg(i++, i_prob_lastname3_buf);
        kernel_1_r.setArg(i++, i_prob_firstname1_buf);
        kernel_1_r.setArg(i++, i_prob_firstname2_buf);
        kernel_1_r.setArg(i++, i_prob_firstname3_buf);
        kernel_1_r.setArg(i++, i_prob_birthname1_buf);
        kernel_1_r.setArg(i++, i_prob_birthname2_buf);
        kernel_1_r.setArg(i++, i_prob_birthname3_buf);
        kernel_1_r.setArg(i++, i_prob_birthday_buf);
        kernel_1_r.setArg(i++, i_prob_gender_buf);
        kernel_1_r.setArg(i++, i_prob_birthmonth_buf);
        kernel_1_r.setArg(i++, i_prob_birthyear_buf);
        kernel_1_r.setArg(i++, i_prob_cin_buf);
        kernel_1_r.setArg(i++, match_id_res_g_buf);
        kernel_1_r.setArg(i++, match_weight_res_g_buf);
        kernel_1_r.setArg(i++, id_measure_res_g_buf);
        kernel_1_r.setArg(i++, match_id_int_res_buf);
        kernel_1_r.setArg(i++, match_weight_int_res_buf);
        kernel_1_r.setArg(i++, id_measure_int_res_buf);
    }

    cl::Kernel kernel_1_lr;
    if ((NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS != 0) && (NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS != 0)) {
        // build first kernel for last batch in l- and r-dim
        cl::Program _program_1_lr(_context,
                                  cl::Program::Sources(1, std::make_pair(kernel_source_1.c_str(),
                                                                         kernel_source_1.length()))
        );
        std::stringstream flags_lr;
        auto configuration_lr = configuration;
        configuration_lr["G_CB_SIZE_L_1"] = NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS;
        configuration_lr["G_CB_SIZE_R_1"] = NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS;
        configuration_lr["L_CB_SIZE_L_1"] = ((NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS) + configuration["NUM_WG_L_1"] - 1) / configuration["NUM_WG_L_1"];
        configuration_lr["L_CB_SIZE_R_1"] = ((NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS) + configuration["NUM_WG_R_1"] - 1) / configuration["NUM_WG_R_1"];
        for (const auto tp : configuration_lr) {
            flags_lr << " -D" << tp.first << "=" << tp.second;
        }
        chrono_start = std::chrono::system_clock::now();
        error = _program_1_lr.build(std::vector<cl::Device>(1, _device), flags_lr.str().c_str());
        if (error != CL_SUCCESS) {
            std::cout << flags_lr.str() << std::endl;
            std::cout << error << std::endl;
            if (error == CL_BUILD_PROGRAM_FAILURE) {
                auto buildLog = _program_1_lr.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
                check_error(error);
                std::cout << std::endl << "Build of first kernel for last batch in l- and r-dim failed! Log:" << std::endl << buildLog << std::endl;
            }
            exit(EXIT_FAILURE);
        }
        chrono_end = std::chrono::system_clock::now();
        runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//        std::cout << "compilation time for first kernel for last batch in l- and r-dim: " << runtime_in_sec << "ms" << std::endl;
        buildLog = _program_1_lr.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
        check_error(error);
//        std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
        kernel_1_lr = cl::Kernel(_program_1_lr, kernel_name_1.c_str(), &error);
        check_error(error);
        i = 0;
        kernel_1_lr.setArg(i++, probM);
        kernel_1_lr.setArg(i++, n_id_buf);
        kernel_1_lr.setArg(i++, n_lastname1_buf);
        kernel_1_lr.setArg(i++, n_lastname2_buf);
        kernel_1_lr.setArg(i++, n_lastname3_buf);
        kernel_1_lr.setArg(i++, n_firstname1_buf);
        kernel_1_lr.setArg(i++, n_firstname2_buf);
        kernel_1_lr.setArg(i++, n_firstname3_buf);
        kernel_1_lr.setArg(i++, n_firstname_group1_buf);
        kernel_1_lr.setArg(i++, n_firstname_group2_buf);
        kernel_1_lr.setArg(i++, n_firstname_group3_buf);
        kernel_1_lr.setArg(i++, n_birthname1_buf);
        kernel_1_lr.setArg(i++, n_birthname2_buf);
        kernel_1_lr.setArg(i++, n_birthname3_buf);
        kernel_1_lr.setArg(i++, n_birthday_buf);
        kernel_1_lr.setArg(i++, n_gender_buf);
        kernel_1_lr.setArg(i++, n_birthmonth_buf);
        kernel_1_lr.setArg(i++, n_birthyear_buf);
        kernel_1_lr.setArg(i++, n_cin_buf);
        kernel_1_lr.setArg(i++, n_prob_lastname1_buf);
        kernel_1_lr.setArg(i++, n_prob_lastname2_buf);
        kernel_1_lr.setArg(i++, n_prob_lastname3_buf);
        kernel_1_lr.setArg(i++, n_prob_firstname1_buf);
        kernel_1_lr.setArg(i++, n_prob_firstname2_buf);
        kernel_1_lr.setArg(i++, n_prob_firstname3_buf);
        kernel_1_lr.setArg(i++, n_prob_birthname1_buf);
        kernel_1_lr.setArg(i++, n_prob_birthname2_buf);
        kernel_1_lr.setArg(i++, n_prob_birthname3_buf);
        kernel_1_lr.setArg(i++, n_prob_birthday_buf);
        kernel_1_lr.setArg(i++, n_prob_gender_buf);
        kernel_1_lr.setArg(i++, n_prob_birthmonth_buf);
        kernel_1_lr.setArg(i++, n_prob_birthyear_buf);
        kernel_1_lr.setArg(i++, n_prob_cin_buf);
        kernel_1_lr.setArg(i++, i_id_buf);
        kernel_1_lr.setArg(i++, i_lastname1_buf);
        kernel_1_lr.setArg(i++, i_lastname2_buf);
        kernel_1_lr.setArg(i++, i_lastname3_buf);
        kernel_1_lr.setArg(i++, i_firstname1_buf);
        kernel_1_lr.setArg(i++, i_firstname2_buf);
        kernel_1_lr.setArg(i++, i_firstname3_buf);
        kernel_1_lr.setArg(i++, i_firstname_group1_buf);
        kernel_1_lr.setArg(i++, i_firstname_group2_buf);
        kernel_1_lr.setArg(i++, i_firstname_group3_buf);
        kernel_1_lr.setArg(i++, i_birthname1_buf);
        kernel_1_lr.setArg(i++, i_birthname2_buf);
        kernel_1_lr.setArg(i++, i_birthname3_buf);
        kernel_1_lr.setArg(i++, i_birthday_buf);
        kernel_1_lr.setArg(i++, i_gender_buf);
        kernel_1_lr.setArg(i++, i_birthmonth_buf);
        kernel_1_lr.setArg(i++, i_birthyear_buf);
        kernel_1_lr.setArg(i++, i_cin_buf);
        kernel_1_lr.setArg(i++, i_prob_lastname1_buf);
        kernel_1_lr.setArg(i++, i_prob_lastname2_buf);
        kernel_1_lr.setArg(i++, i_prob_lastname3_buf);
        kernel_1_lr.setArg(i++, i_prob_firstname1_buf);
        kernel_1_lr.setArg(i++, i_prob_firstname2_buf);
        kernel_1_lr.setArg(i++, i_prob_firstname3_buf);
        kernel_1_lr.setArg(i++, i_prob_birthname1_buf);
        kernel_1_lr.setArg(i++, i_prob_birthname2_buf);
        kernel_1_lr.setArg(i++, i_prob_birthname3_buf);
        kernel_1_lr.setArg(i++, i_prob_birthday_buf);
        kernel_1_lr.setArg(i++, i_prob_gender_buf);
        kernel_1_lr.setArg(i++, i_prob_birthmonth_buf);
        kernel_1_lr.setArg(i++, i_prob_birthyear_buf);
        kernel_1_lr.setArg(i++, i_prob_cin_buf);
        kernel_1_lr.setArg(i++, match_id_res_g_buf);
        kernel_1_lr.setArg(i++, match_weight_res_g_buf);
        kernel_1_lr.setArg(i++, id_measure_res_g_buf);
        kernel_1_lr.setArg(i++, match_id_int_res_buf);
        kernel_1_lr.setArg(i++, match_weight_int_res_buf);
        kernel_1_lr.setArg(i++, id_measure_int_res_buf);
    }

    cl::Program _program_2(_context,
                           cl::Program::Sources(1, std::make_pair(kernel_source_2.c_str(),
                                                                  kernel_source_2.length()))
    );
    cl::Kernel kernel_2;
    cl::Program _program_2_l(_context,
                             cl::Program::Sources(1, std::make_pair(kernel_source_2.c_str(),
                                                                    kernel_source_2.length()))
    );
    cl::Kernel kernel_2_l;
    if (configuration["NUM_WG_R_1"] > 1) {
        // build second kernel
        chrono_start = std::chrono::system_clock::now();
        error = _program_2.build(std::vector<cl::Device>(1, _device), flags.str().c_str());
        if (error != CL_SUCCESS) {
            std::cout << flags.str() << std::endl;
            std::cout << error << std::endl;
            if (error == CL_BUILD_PROGRAM_FAILURE) {
                buildLog = _program_2.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
                std::cout << std::endl << "Build of second kernel failed! Log:" << std::endl << buildLog << std::endl;
            }
            exit(EXIT_FAILURE);
        }
        chrono_end = std::chrono::system_clock::now();
        runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//        std::cout << "compilation time for second kernel: " << runtime_in_sec << "ms" << std::endl;
        buildLog = _program_2.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error); check_error(error);
//        std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
        kernel_2 = cl::Kernel(_program_2, kernel_name_2.c_str(), &error); check_error(error);
        kernel_2.setArg(0, match_id_int_res_buf);
        kernel_2.setArg(1, match_weight_int_res_buf);
        kernel_2.setArg(2, id_measure_int_res_buf);
        kernel_2.setArg(3, match_id_res_g_buf);
        kernel_2.setArg(4, match_weight_res_g_buf);
        kernel_2.setArg(5, id_measure_res_g_buf);
        kernel_2.setArg(6, match_id_res_buf);
        kernel_2.setArg(7, match_weight_res_buf);
        kernel_2.setArg(8, id_measure_res_buf);

        if (NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS != 0) {
            // build first kernel for last batch in l-dim
            chrono_start = std::chrono::system_clock::now();
            error = _program_2_l.build(std::vector<cl::Device>(1, _device), flags_l.str().c_str());
            if (error != CL_SUCCESS) {
                std::cout << flags_l.str() << std::endl;
                std::cout << error << std::endl;
                if (error == CL_BUILD_PROGRAM_FAILURE) {
                    buildLog = _program_2_l.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
                    check_error(error);
                    std::cout << std::endl << "Build of second kernel failed! Log:" << std::endl << buildLog << std::endl;
                }
                exit(EXIT_FAILURE);
            }
            chrono_end = std::chrono::system_clock::now();
            runtime_in_sec = std::chrono::duration_cast<std::chrono::milliseconds>(chrono_end - chrono_start).count();
//            std::cout << "compilation time for second kernel for last batch in l-dim: " << runtime_in_sec << "ms" << std::endl;
            buildLog = _program_2_l.getBuildInfo<CL_PROGRAM_BUILD_LOG>(_device, &error);
            check_error(error);
//            std::cout << std::endl << "Log:" << std::endl << buildLog << std::endl;
            kernel_2_l = cl::Kernel(_program_2_l, kernel_name_2.c_str(), &error);
            check_error(error);
            kernel_2_l.setArg(0, match_id_int_res_buf);
            kernel_2_l.setArg(1, match_weight_int_res_buf);
            kernel_2_l.setArg(2, id_measure_int_res_buf);
            kernel_2_l.setArg(3, match_id_res_g_buf);
            kernel_2_l.setArg(4, match_weight_res_g_buf);
            kernel_2_l.setArg(5, id_measure_res_g_buf);
            kernel_2_l.setArg(6, match_id_res_buf);
            kernel_2_l.setArg(7, match_weight_res_buf);
            kernel_2_l.setArg(8, id_measure_res_buf);
        }
    }

    // calculate global & local size
    size_t gs_0_1 = 1, gs_1_1 = 1, gs_2_1 = 1;
    size_t ls_0_1 = 1, ls_1_1 = 1, ls_2_1 = 1;
    if (configuration.find("OCL_DIM_L_1") != configuration.end()) {
        size_t tmp_gs = static_cast<size_t>(configuration["NUM_WG_L_1"] * configuration["NUM_WI_L_1"]);
        size_t tmp_ls = static_cast<size_t>(configuration["NUM_WI_L_1"]);
        if (configuration["OCL_DIM_L_1"] == 0) {
            gs_0_1 = tmp_gs;
            ls_0_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_1"] == 1) {
            gs_1_1 = tmp_gs;
            ls_1_1 = tmp_ls;
        } else if (configuration["OCL_DIM_L_1"] == 2) {
            gs_2_1 = tmp_gs;
            ls_2_1 = tmp_ls;
        }
    }
    if (configuration.find("OCL_DIM_R_1") != configuration.end()) {
        size_t tmp_gs = static_cast<size_t>(configuration["NUM_WG_R_1"] * configuration["NUM_WI_R_1"]);
        size_t tmp_ls = static_cast<size_t>(configuration["NUM_WI_R_1"]);
        if (configuration["OCL_DIM_R_1"] == 0) {
            gs_0_1 = tmp_gs;
            ls_0_1 = tmp_ls;
        } else if (configuration["OCL_DIM_R_1"] == 1) {
            gs_1_1 = tmp_gs;
            ls_1_1 = tmp_ls;
        } else if (configuration["OCL_DIM_R_1"] == 2) {
            gs_2_1 = tmp_gs;
            ls_2_1 = tmp_ls;
        }
    }
    cl::NDRange global_size_1(gs_0_1, gs_1_1, gs_2_1);
    cl::NDRange local_size_1(ls_0_1, ls_1_1, ls_2_1);
    cl::NDRange global_size_2;
    cl::NDRange local_size_2;
    if (configuration["NUM_WG_R_1"] > 1 && configuration.find("OCL_DIM_R_1") != configuration.end()) {
        size_t tmp_gs = (size_t) std::min(configuration["NUM_WI_R_1"], ceil_lambda(std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))), std::min(configuration["P_CB_SIZE_R_1"], std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))))));
        size_t tmp_ls = (size_t) std::min(configuration["NUM_WI_R_1"], ceil_lambda(std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))), std::min(configuration["P_CB_SIZE_R_1"], std::min(configuration["L_CB_SIZE_R_1"], std::min(configuration["NUM_WG_R_1"], ceil_lambda(configuration["G_CB_SIZE_R_1"], configuration["L_CB_SIZE_R_1"]))))));
        if (configuration["OCL_DIM_R_1"] == 0) {
            gs_0_1 = tmp_gs;
            ls_0_1 = tmp_ls;
        } else if (configuration["OCL_DIM_R_1"] == 1) {
            gs_1_1 = tmp_gs;
            ls_1_1 = tmp_ls;
        } else if (configuration["OCL_DIM_R_1"] == 2) {
            gs_2_1 = tmp_gs;
            ls_2_1 = tmp_ls;
        }
        global_size_2 = cl::NDRange(gs_0_1, gs_1_1, gs_2_1);
        local_size_2 = cl::NDRange(ls_0_1, ls_1_1, ls_2_1);
    }

    cl::Event event;
    cl_ulong start, end;
    cl_ulong events_total_kernel_1 = 0;
    cl_ulong events_total_kernel_2 = 0;
    cl_ulong events_total_transfer = 0;
    size_t num_new_reports = BATCH_SIZE_NEW_REPORTS;
    size_t num_existing_reports = BATCH_SIZE_EXISTING_REPORTS;
    auto *match_id_batch_result = new long[BATCH_SIZE_NEW_REPORTS];
    if (match_id_batch_result == nullptr) std::cout << "could not allocate match_id_batch_result" << std::endl;
    auto *match_weight_batch_result = new double[BATCH_SIZE_NEW_REPORTS];
    if (match_weight_batch_result == nullptr) std::cout << "could not allocate match_weight_batch_result" << std::endl;
    auto *id_measure_batch_result = new int[BATCH_SIZE_NEW_REPORTS];
    if (id_measure_batch_result == nullptr) std::cout << "could not allocate id_measure_batch_result" << std::endl;
    ctpl::thread_pool pool(NUM_THREADS);
    std::vector<std::future<void>> results(NUM_THREADS);
//    std::cout << "Starting batch processing" << std::endl;
    auto chrono_total_start = std::chrono::high_resolution_clock::now();
    for (size_t batch_existing_reports = 0; batch_existing_reports < (NUM_EXISTING_REPORTS + BATCH_SIZE_EXISTING_REPORTS - 1) / BATCH_SIZE_EXISTING_REPORTS; ++batch_existing_reports) {
        if (NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS != 0) {
            if (batch_existing_reports == (NUM_EXISTING_REPORTS + BATCH_SIZE_EXISTING_REPORTS - 1) / BATCH_SIZE_EXISTING_REPORTS - 1) {
                num_existing_reports = NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS;
            }
        }

        // copy buffer
        error = _command_queue.enqueueWriteBuffer(i_id_buf, CL_TRUE, 0, num_existing_reports * sizeof(long), i_id.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_lastname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_lastname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_lastname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_lastname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_lastname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_lastname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_firstname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_firstname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_firstname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname_group1_buf, CL_TRUE, 0, num_existing_reports * sizeof(long), i_firstname_group1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname_group2_buf, CL_TRUE, 0, num_existing_reports * sizeof(long), i_firstname_group2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_firstname_group3_buf, CL_TRUE, 0, num_existing_reports * sizeof(long), i_firstname_group3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_birthname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_birthname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_birthname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthday_buf, CL_TRUE, 0, num_existing_reports * sizeof(str46), i_birthday.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_gender_buf, CL_TRUE, 0, num_existing_reports * sizeof(chr2), i_gender.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthmonth_buf, CL_TRUE, 0, num_existing_reports * sizeof(int), i_birthmonth.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_birthyear_buf, CL_TRUE, 0, num_existing_reports * sizeof(int), i_birthyear.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_cin_buf, CL_TRUE, 0, num_existing_reports * sizeof(int), i_cin.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_lastname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_lastname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_lastname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_lastname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_lastname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_lastname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_firstname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_firstname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_firstname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_firstname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_firstname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_firstname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthname1_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthname1.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthname2_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthname2.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthname3_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthname3.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthday_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthday.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_gender_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_gender.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthmonth_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthmonth.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_birthyear_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_birthyear.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
        error = _command_queue.enqueueWriteBuffer(i_prob_cin_buf, CL_TRUE, 0, num_existing_reports * sizeof(double), i_prob_cin.data() + batch_existing_reports * BATCH_SIZE_EXISTING_REPORTS, nullptr, &event); check_error(error);
        check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;

        num_new_reports = BATCH_SIZE_NEW_REPORTS;
        for (size_t batch_new_reports = 0; batch_new_reports < (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS; ++batch_new_reports) {
            if (NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS != 0) {
                if (batch_new_reports == (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS - 1) {
                    num_new_reports = NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS;
                }
            }

            // copy buffer
            error = _command_queue.enqueueWriteBuffer(n_id_buf, CL_TRUE, 0, num_new_reports * sizeof(long), n_id.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_lastname1_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_lastname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_lastname2_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_lastname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_lastname3_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_lastname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname1_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_firstname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname2_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_firstname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname3_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_firstname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname_group1_buf, CL_TRUE, 0, num_new_reports * sizeof(long), n_firstname_group1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname_group2_buf, CL_TRUE, 0, num_new_reports * sizeof(long), n_firstname_group2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_firstname_group3_buf, CL_TRUE, 0, num_new_reports * sizeof(long), n_firstname_group3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthname1_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_birthname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthname2_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_birthname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthname3_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_birthname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthday_buf, CL_TRUE, 0, num_new_reports * sizeof(str46), n_birthday.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_gender_buf, CL_TRUE, 0, num_new_reports * sizeof(chr2), n_gender.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthmonth_buf, CL_TRUE, 0, num_new_reports * sizeof(int), n_birthmonth.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_birthyear_buf, CL_TRUE, 0, num_new_reports * sizeof(int), n_birthyear.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_cin_buf, CL_TRUE, 0, num_new_reports * sizeof(int), n_cin.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_lastname1_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_lastname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_lastname2_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_lastname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_lastname3_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_lastname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_firstname1_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_firstname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_firstname2_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_firstname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_firstname3_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_firstname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthname1_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthname1.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthname2_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthname2.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthname3_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthname3.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthday_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthday.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_gender_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_gender.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthmonth_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthmonth.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_birthyear_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_birthyear.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;
            error = _command_queue.enqueueWriteBuffer(n_prob_cin_buf, CL_TRUE, 0, num_new_reports * sizeof(double), n_prob_cin.data() + batch_new_reports * BATCH_SIZE_NEW_REPORTS, nullptr, &event); check_error(error);
            check_error(event.wait()); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start)); check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end)); events_total_transfer += end - start;

            // execute kernel
            if ((NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS == 0) && (NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS == 0)) {
                error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
            } else if (NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS != 0) {
                if (batch_new_reports == (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS - 1) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1_l, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                } else {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                }
            } else if (NUM_EXISTING_REPORTS % BATCH_SIZE_EXISTING_REPORTS != 0) {
                if (batch_existing_reports == (NUM_EXISTING_REPORTS + BATCH_SIZE_EXISTING_REPORTS - 1) / BATCH_SIZE_EXISTING_REPORTS - 1) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1_r, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                } else {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                }
            } else {
                if ((batch_existing_reports == (NUM_EXISTING_REPORTS + BATCH_SIZE_EXISTING_REPORTS - 1) / BATCH_SIZE_EXISTING_REPORTS - 1) && (batch_new_reports == (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS - 1)) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1_lr, cl::NullRange, global_size_1, local_size_1, NULL, &event);
                    check_error(error);
                } else if (batch_existing_reports == (NUM_EXISTING_REPORTS + BATCH_SIZE_EXISTING_REPORTS - 1) / BATCH_SIZE_EXISTING_REPORTS - 1) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1_r, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                } else if (batch_new_reports == (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS - 1) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1_l, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                } else {
                    error = _command_queue.enqueueNDRangeKernel(kernel_1, cl::NullRange, global_size_1, local_size_1, NULL, &event); check_error(error);
                }
            }
            check_error(event.wait());
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
            events_total_kernel_1 += end - start;

            if (configuration["NUM_WG_R_1"] > 1) {
                if (NUM_NEW_REPORTS % BATCH_SIZE_NEW_REPORTS == 0) {
                    error = _command_queue.enqueueNDRangeKernel(kernel_2, cl::NullRange, global_size_2, local_size_2, NULL, &event); check_error(error);
                } else {
                    if (batch_new_reports == (NUM_NEW_REPORTS + BATCH_SIZE_NEW_REPORTS - 1) / BATCH_SIZE_NEW_REPORTS - 1) {
                        error = _command_queue.enqueueNDRangeKernel(kernel_2_l, cl::NullRange, global_size_2, local_size_2, NULL, &event); check_error(error);
                    } else {
                        error = _command_queue.enqueueNDRangeKernel(kernel_2, cl::NullRange, global_size_2, local_size_2, NULL, &event); check_error(error);
                    }
                }
                check_error(event.wait());
                check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
                check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
                events_total_kernel_2 += end - start;
            }

            // copy results
            error = _command_queue.enqueueReadBuffer((configuration["NUM_WG_R_1"] > 1 ? match_id_res_buf : match_id_int_res_buf), CL_TRUE, 0, num_new_reports * sizeof(long), match_id_batch_result, nullptr, &event); check_error(error);
            check_error(event.wait());
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
            events_total_transfer += end - start;
            error = _command_queue.enqueueReadBuffer((configuration["NUM_WG_R_1"] > 1 ? match_weight_res_buf : match_weight_int_res_buf), CL_TRUE, 0, num_new_reports * sizeof(double), match_weight_batch_result, nullptr, &event); check_error(error);
            check_error(event.wait());
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
            events_total_transfer += end - start;
            error = _command_queue.enqueueReadBuffer((configuration["NUM_WG_R_1"] > 1 ? id_measure_res_buf : id_measure_int_res_buf), CL_TRUE, 0, num_new_reports * sizeof(int), id_measure_batch_result, nullptr, &event); check_error(error);
            check_error(event.wait());
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start));
            check_error(event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end));
            events_total_transfer += end - start;

            // combine results
            for (int thread_id = 0; thread_id < std::min(NUM_THREADS, (int)num_new_reports); ++thread_id) {
                results[thread_id] = pool.push(
                        [&res_match_id, &res_match_weight, &res_id_measure, &match_id_batch_result, &match_weight_batch_result, &id_measure_batch_result, &num_new_reports, &batch_new_reports, &NUM_THREADS]
                                (int id) {
                            for (size_t neue_meldung = id * (num_new_reports / NUM_THREADS); neue_meldung < (id + 1) * (num_new_reports / NUM_THREADS); ++neue_meldung) {
                                if (res_id_measure[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung] != 14
                                    && res_match_weight[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung] <= 45.0
                                    && ((id_measure_batch_result[neue_meldung] == 14)
                                        || (match_weight_batch_result[neue_meldung] > res_match_weight[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung]))) {
                                    res_match_id[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung] = match_id_batch_result[neue_meldung];
                                    res_match_weight[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung] = match_weight_batch_result[neue_meldung];
                                    res_id_measure[batch_new_reports * BATCH_SIZE_NEW_REPORTS + neue_meldung] = id_measure_batch_result[neue_meldung];
                                }
                            }
                        });
            }
            for (int thread_id = 0; thread_id < std::min(NUM_THREADS, (int)num_new_reports); ++thread_id) {
                results[thread_id].get();
            }
        }
    }
    auto chrono_total_end = std::chrono::high_resolution_clock::now();

    std::string s;
    s = std::to_string(std::chrono::duration_cast<std::chrono::milliseconds>(chrono_total_end - chrono_total_start).count());
    s += " ms ";
    while (s.length() < 18) s = " " + s;
    std::cout << s;
//    std::cout << "total runtime (chrono): " << std::chrono::duration_cast<std::chrono::milliseconds>(chrono_total_end - chrono_total_start).count() << " ms" << std::endl;
//    std::cout << "total runtime transfer: " << events_total_transfer / 1000000 << " ms" << std::endl;
//    std::cout << "total runtime kernel_1: " << events_total_kernel_1 / 1000000 << " ms" << std::endl;
//    std::cout << "total runtime kernel_2: " << events_total_kernel_2 << " ns" << std::endl;

    return 0;
}