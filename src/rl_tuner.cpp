#define READ_DATA

typedef struct {
    char str[46];
} str46;
typedef struct {
    char values[2];
} chr2;
typedef struct {
    double values[8];
} dbl8;

#include <iostream>
#include "../libraries/ATF/atf.h"
#include "../libraries/argparse.hpp"
#include <fstream>
#include <map>
#include <sstream>
#include <chrono>
#include <cassert>
#include <unordered_map>
#include <iomanip>


void check_error(cl_int err) {
    if (err != CL_SUCCESS) {
        printf("Error with errorcode: %d\n", err);
        exit(EXIT_FAILURE);
    }
}

int main(int argc, const char *argv[]) {
    ArgumentParser args;
    args.appName("rl_tuner");
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id", 1, false);
    args.addArgument("--num-new-reports", 1, false);
    args.addArgument("--num-existing-reports", 1, false);
    args.parse(static_cast<size_t>(argc), argv);
    const int NUM_NEW_REPORTS = args.retrieve_int("num-new-reports");
    const int NUM_EXISTING_REPORTS = args.retrieve_int("num-existing-reports");
    const int PLATFORM_ID = args.retrieve_int("platform-id");
    const int DEVICE_ID = args.retrieve_int("device-id");

    auto tuner = atf::exhaustive();

    const dbl8 probM =
            {{
                     0.99439781, // lastname
                     0.98247962, // firstname
                     0.98772576, // birthname
                     0.99949391, // birthday
                     0.9995371,  // birthmonth
                     0.99971322, // birthyear
                     0.99999304, // gender
                     0.9915867   // cid
             }};

    std::vector<long>      n_id(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_lastname3(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_firstname3(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group1(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group2(NUM_NEW_REPORTS);
    std::vector<long>      n_firstname_group3(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname1(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname2(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthname3(NUM_NEW_REPORTS);
    std::vector<str46>     n_birthday(NUM_NEW_REPORTS);
    std::vector<chr2>      n_gender(NUM_NEW_REPORTS);
    std::vector<int>       n_birthmonth(NUM_NEW_REPORTS);
    std::vector<int>       n_birthyear(NUM_NEW_REPORTS);
    std::vector<int>       n_cin(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_lastname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_firstname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname1(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname2(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthname3(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthday(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_gender(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthmonth(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_birthyear(NUM_NEW_REPORTS);
    std::vector<double>    n_prob_cin(NUM_NEW_REPORTS);

#ifdef READ_DATA
    std::string n_file_name = "data/n_" + std::to_string(NUM_NEW_REPORTS) + ".csv";
    std::cout << "loading new reports from " << n_file_name << std::endl;
    std::unordered_map<std::string, long> firstname_groups;
    std::ifstream n_csv_file(n_file_name);
    std::string line;
    int li = 0;
    while (std::getline(n_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> n_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(n_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> n_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(n_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> n_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(n_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> n_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(n_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> n_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(n_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> n_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(n_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> n_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(n_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> n_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(n_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> n_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(n_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> n_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(n_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> n_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> n_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> n_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> n_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> n_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(n_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> n_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> n_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> n_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (n_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname1[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group1[li] = firstname_groups[std::string(n_firstname1[li].str, 46)];
        }
        if (n_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname2[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group2[li] = firstname_groups[std::string(n_firstname2[li].str, 46)];
        }
        if (n_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname3[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group3[li] = firstname_groups[std::string(n_firstname3[li].str, 46)];
        }
        n_prob_birthyear[li] = 1.0E-7;
        n_prob_gender[li] = 1.0E-7;
        n_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == NUM_NEW_REPORTS);
    n_csv_file.close();

#endif

    std::vector<long>      i_id(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_lastname3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_firstname3(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group1(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group2(NUM_EXISTING_REPORTS);
    std::vector<long>      i_firstname_group3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname1(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname2(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthname3(NUM_EXISTING_REPORTS);
    std::vector<str46>     i_birthday(NUM_EXISTING_REPORTS);
    std::vector<chr2>      i_gender(NUM_EXISTING_REPORTS);
    std::vector<int>       i_birthmonth(NUM_EXISTING_REPORTS);
    std::vector<int>       i_birthyear(NUM_EXISTING_REPORTS);
    std::vector<int>       i_cin(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_lastname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_firstname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname1(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname2(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthname3(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthday(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_gender(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthmonth(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_birthyear(NUM_EXISTING_REPORTS);
    std::vector<double>    i_prob_cin(NUM_EXISTING_REPORTS);

#ifdef READ_DATA
    std::string i_file_name = "data/i_" + std::to_string(NUM_EXISTING_REPORTS) + ".csv";
    std::cout << "loading inventory reports from " << i_file_name << std::endl;
    std::ifstream i_csv_file(i_file_name);
    li = 0;
    while (std::getline(i_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> i_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(i_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> i_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(i_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> i_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(i_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> i_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(i_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> i_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(i_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> i_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(i_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> i_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(i_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> i_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(i_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> i_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(i_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> i_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(i_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> i_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> i_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> i_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> i_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> i_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(i_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> i_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> i_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> i_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (i_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname1[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group1[li] = firstname_groups[std::string(i_firstname1[li].str, 46)];
        }
        if (i_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname2[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group2[li] = firstname_groups[std::string(i_firstname2[li].str, 46)];
        }
        if (i_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname3[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group3[li] = firstname_groups[std::string(i_firstname3[li].str, 46)];
        }
        i_prob_birthyear[li] = 1.0E-7;
        i_prob_gender[li] = 1.0E-7;
        i_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == NUM_EXISTING_REPORTS);
    i_csv_file.close();
#endif

    std::vector<long> res_match_id(NUM_NEW_REPORTS);
    std::vector<double> res_match_weight(NUM_NEW_REPORTS);
    std::vector<int> res_id_measure(NUM_NEW_REPORTS);

    auto device = atf::cf::device_info(PLATFORM_ID, DEVICE_ID);
    device.initialize();

    // get device specific limits
    std::vector<size_t> max_wi_size;
    size_t max_wg_size;
    cl_ulong max_local_mem_size;
    cl_uint dims;
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &dims));
    max_wi_size = std::vector<size_t>(dims);
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &max_wi_size));
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &max_wg_size));
    atf::cf::check_error(device.device().getInfo(CL_DEVICE_LOCAL_MEM_SIZE, &max_local_mem_size));
    const size_t combined_max_wi_size = *std::max_element(max_wi_size.begin(), max_wi_size.end());

    // fixed tp values
    auto TP_G_CB_SIZE_L_1       = atf::tp("G_CB_SIZE_L_1",       {(NUM_NEW_REPORTS)});
    auto TP_G_CB_SIZE_R_1       = atf::tp("G_CB_SIZE_R_1",       {(NUM_EXISTING_REPORTS)});
    auto TP_CACHE_L_CB          = atf::tp("CACHE_L_CB",          {(0)});
    auto TP_CACHE_P_CB          = atf::tp("CACHE_P_CB",          {(0)});
    auto TP_G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {(2)});
    auto TP_L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {(1)});
    auto TP_P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {(0)});
    auto TP_OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {(1)});
    auto TP_OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {(0)});
    auto TP_P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {(1)});
    auto TP_P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {(1)});

    // tuneable parameters
    auto TP_NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval((size_t)0, (size_t)ceil(log2((size_t)NUM_NEW_REPORTS)), atf::pow_2));
    auto TP_NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          atf::interval((size_t)0, (size_t)ceil(log2((size_t)NUM_EXISTING_REPORTS)), atf::pow_2));
    auto TP_NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval((size_t)0, (size_t)ceil(log2(std::min((size_t)(NUM_NEW_REPORTS), max_wi_size[1]))), atf::pow_2));
    auto TP_NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval((size_t)0, (size_t)ceil(log2(std::min((size_t)(NUM_EXISTING_REPORTS), max_wi_size[0]))), atf::pow_2), [&](auto TP_NUM_WI_R_1) { return TP_NUM_WI_L_1 * TP_NUM_WI_R_1 <= max_wg_size; });

    // parameters dependend on tuneable parameters
    auto TP_L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval(1, NUM_NEW_REPORTS), atf::equal((NUM_NEW_REPORTS + TP_NUM_WG_L_1 - 1) / TP_NUM_WG_L_1) && atf::less_than_or_eq(max_local_mem_size / (sizeof(long) + sizeof(double) + sizeof(int)) / TP_NUM_WI_R_1));
    auto TP_L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       atf::interval(1, NUM_EXISTING_REPORTS), atf::equal((NUM_EXISTING_REPORTS + TP_NUM_WG_R_1 - 1) / TP_NUM_WG_R_1));

    tuner(TP_G_CB_SIZE_L_1, TP_G_CB_SIZE_R_1,
          TP_CACHE_L_CB, TP_CACHE_P_CB,
          TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL,
          TP_OCL_DIM_L_1, TP_OCL_DIM_R_1,
          TP_P_CB_SIZE_L_1, TP_P_CB_SIZE_R_1);
    tuner(TP_NUM_WG_L_1, TP_NUM_WG_R_1, TP_NUM_WI_L_1, TP_NUM_WI_R_1, TP_L_CB_SIZE_L_1, TP_L_CB_SIZE_R_1);

    std::vector<char> int_res(NUM_NEW_REPORTS);
    auto calc_res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        size_t size = NUM_NEW_REPORTS * sizeof(char);
        if (kernel == 1) {
            if (config[TP_G_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[TP_NUM_WG_R_1.name()].value().size_t_val();
            }
            if (config[TP_L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[TP_NUM_WI_R_1.name()].value().size_t_val();
            }
        } else {
            if (config[TP_L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= std::min(config[TP_NUM_WI_R_1.name()].value().size_t_val(), atf::ceil_lambda(std::min(config[TP_L_CB_SIZE_R_1.name()].value().size_t_val(), std::min(config[TP_NUM_WG_R_1.name()].value().size_t_val(), atf::ceil_lambda(config[TP_G_CB_SIZE_R_1.name()].value().size_t_val(), config[TP_L_CB_SIZE_R_1.name()].value().size_t_val()))), std::min(config[TP_P_CB_SIZE_R_1.name()].value().size_t_val(), std::min(config[TP_L_CB_SIZE_R_1.name()].value().size_t_val(), std::min(config[TP_NUM_WG_R_1.name()].value().size_t_val(), atf::ceil_lambda(config[TP_G_CB_SIZE_R_1.name()].value().size_t_val(), config[TP_L_CB_SIZE_R_1.name()].value().size_t_val()))))));
            }
        }
        if (config[TP_P_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
            size *= 1;
        }
        return size;
    };
    auto calc_int_res_size = [&](atf::configuration &config) -> size_t {
        size_t size = NUM_NEW_REPORTS * config[TP_NUM_WG_R_1.name()].value().size_t_val() * sizeof(char);
        return size;
    };
    auto needs_second_kernel = [&](atf::configuration &config) -> bool {
        return config[TP_NUM_WG_R_1.name()].value().size_t_val() > 1;
    };
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    atf::cf::process_wrapper_info process_wrapper_info = {
            atf::cf::NONE,
            "", "", ""
    };
    atf::cf::timeout warm_up_timeout = {atf::cf::ABSOLUTE, {.absolute = 0}};
    atf::cf::timeout evaluation_timeout = {atf::cf::ABSOLUTE, {.absolute = 0}};
    auto wrapper = atf::cf::ocl_md_hom(
            device,
            {atf::cf::kernel_info::FILENAME, "rl_1.cl", "rl_1", ""},
            atf::inputs(atf::scalar<dbl8>(probM),
                        atf::buffer(n_id),
                        atf::buffer(n_lastname1),
                        atf::buffer(n_lastname2),
                        atf::buffer(n_lastname3),
                        atf::buffer(n_firstname1),
                        atf::buffer(n_firstname2),
                        atf::buffer(n_firstname3),
                        atf::buffer(n_firstname_group1),
                        atf::buffer(n_firstname_group2),
                        atf::buffer(n_firstname_group3),
                        atf::buffer(n_birthname1),
                        atf::buffer(n_birthname2),
                        atf::buffer(n_birthname3),
                        atf::buffer(n_birthday),
                        atf::buffer(n_gender),
                        atf::buffer(n_birthmonth),
                        atf::buffer(n_birthyear),
                        atf::buffer(n_cin),
                        atf::buffer(n_prob_lastname1),
                        atf::buffer(n_prob_lastname2),
                        atf::buffer(n_prob_lastname3),
                        atf::buffer(n_prob_firstname1),
                        atf::buffer(n_prob_firstname2),
                        atf::buffer(n_prob_firstname3),
                        atf::buffer(n_prob_birthname1),
                        atf::buffer(n_prob_birthname2),
                        atf::buffer(n_prob_birthname3),
                        atf::buffer(n_prob_birthday),
                        atf::buffer(n_prob_gender),
                        atf::buffer(n_prob_birthmonth),
                        atf::buffer(n_prob_birthyear),
                        atf::buffer(n_prob_cin),

                        atf::buffer(i_id),
                        atf::buffer(i_lastname1),
                        atf::buffer(i_lastname2),
                        atf::buffer(i_lastname3),
                        atf::buffer(i_firstname1),
                        atf::buffer(i_firstname2),
                        atf::buffer(i_firstname3),
                        atf::buffer(i_firstname_group1),
                        atf::buffer(i_firstname_group2),
                        atf::buffer(i_firstname_group3),
                        atf::buffer(i_birthname1),
                        atf::buffer(i_birthname2),
                        atf::buffer(i_birthname3),
                        atf::buffer(i_birthday),
                        atf::buffer(i_gender),
                        atf::buffer(i_birthmonth),
                        atf::buffer(i_birthyear),
                        atf::buffer(i_cin),
                        atf::buffer(i_prob_lastname1),
                        atf::buffer(i_prob_lastname2),
                        atf::buffer(i_prob_lastname3),
                        atf::buffer(i_prob_firstname1),
                        atf::buffer(i_prob_firstname2),
                        atf::buffer(i_prob_firstname3),
                        atf::buffer(i_prob_birthname1),
                        atf::buffer(i_prob_birthname2),
                        atf::buffer(i_prob_birthname3),
                        atf::buffer(i_prob_birthday),
                        atf::buffer(i_prob_gender),
                        atf::buffer(i_prob_birthmonth),
                        atf::buffer(i_prob_birthyear),
                        atf::buffer(i_prob_cin)),
            atf::cf::GS(
                    atf::max((TP_OCL_DIM_R_1 == 0) * (TP_NUM_WG_R_1 * TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_R_1 == 1) * (TP_NUM_WG_R_1 * TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_R_1 == 2) * (TP_NUM_WG_R_1 * TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1)
            ),
            atf::cf::LS(
                    atf::max((TP_OCL_DIM_R_1 == 0) * (TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_R_1 == 1) * (TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WI_L_1), 1),

                    atf::max((TP_OCL_DIM_R_1 == 2) * (TP_NUM_WI_R_1)
                             + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WI_L_1), 1)
            ),
            {atf::cf::kernel_info::FILENAME, "rl_2.cl", "rl_2", ""},
            atf::inputs(atf::buffer(res_match_id), atf::buffer(res_match_weight), atf::buffer(res_id_measure)),
            atf::cf::GS(
                    atf::max(  (TP_OCL_DIM_R_1 == 0) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max(  (TP_OCL_DIM_R_1 == 1) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1),

                    atf::max(  (TP_OCL_DIM_R_1 == 2) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WG_L_1 * TP_NUM_WI_L_1), 1)
            ),
            atf::cf::LS(
                    atf::max(  (TP_OCL_DIM_R_1 == 0) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 0) * (TP_NUM_WI_L_1), 1),

                    atf::max(  (TP_OCL_DIM_R_1 == 1) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 1) * (TP_NUM_WI_L_1), 1),

                    atf::max(  (TP_OCL_DIM_R_1 == 2) * (atf::min(TP_NUM_WI_R_1, atf::ceil(atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1))), atf::min(TP_P_CB_SIZE_R_1, atf::min(TP_L_CB_SIZE_R_1, atf::min(TP_NUM_WG_R_1, atf::ceil(TP_G_CB_SIZE_R_1, TP_L_CB_SIZE_R_1)))))))
                               + (TP_OCL_DIM_L_1 == 2) * (TP_NUM_WI_L_1), 1)
            ),
            calc_res_g_size,
            atf::buffer(int_res),
            calc_int_res_size,
            needs_second_kernel,
            is_valid,
            3, 1,
            true,
            process_wrapper_info,
            warm_up_timeout,
            evaluation_timeout
    );

    // start tuning
    auto best_configuration = tuner(wrapper);

    std::ofstream tuned_configuration_file("../tuned_configuration", std::ios::out | std::ios::trunc);
    tuned_configuration_file << "NUM_WG_NEW      " << best_configuration["NUM_WG_L_1"].value().int_val() << std::endl;
    tuned_configuration_file << "NUM_WI_NEW      " << best_configuration["NUM_WI_L_1"].value().int_val() << std::endl;
    tuned_configuration_file << "NUM_WG_EXISTING " << best_configuration["NUM_WG_R_1"].value().int_val() << std::endl;
    tuned_configuration_file << "NUM_WI_EXISTING " << best_configuration["NUM_WI_R_1"].value().int_val() << std::endl;
    tuned_configuration_file.close();

    return 0;
}