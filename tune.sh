#!/bin/bash

cd "$(dirname "$0")/build" &&\
./rl_tuner --platform-id $PLATFORM_ID --device-id $DEVICE_ID --num-new-reports 1024 --num-existing-reports 32768 &&\
echo  &&\
echo  &&\
echo "Successfully tuned for batch size!" &&\
echo
