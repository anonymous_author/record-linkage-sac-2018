#!/bin/bash

#existing=5389590
existing=32768
threads=`nproc --all`
threads=`echo "$threads*2" | bc`
cd "$(dirname "$0")/build" &&\
echo "Linking increasing numbers of new reports against $existing existing reports:"
echo
echo "|   # new reports  |        EKR       |  md_hom (tuned)  | md_hom (untuned) |"
echo "-----------------------------------------------------------------------------"
for i in {15..20}; do
    new=`echo "2^$i" | bc`
    echo -n "|             2^$i "
    echo -n "|"
    java -Xms512m -Xmx14g -jar record-linkage-java.jar $new $existing $threads
    echo -n "|"
    ./rl_batch --platform-id $PLATFORM_ID --device-id $DEVICE_ID\
               --num-threads $threads\
               --num-new-reports $new --num-existing-reports $existing\
               --num-wg-n  `tail -n +1 ../tuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wg-e  `tail -n +3 ../tuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wi-n  `tail -n +2 ../tuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wi-e  `tail -n +4 ../tuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --cb-size-n 0 --cb-size-e 0
    echo -n "|"
    ./rl_batch --platform-id $PLATFORM_ID --device-id $DEVICE_ID\
               --num-threads $threads\
               --num-new-reports $new --num-existing-reports $existing\
               --num-wg-n  `tail -n +1 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wg-e  `tail -n +3 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wi-n  `tail -n +2 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --num-wi-e  `tail -n +4 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --cb-size-n `tail -n +2 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`\
               --cb-size-e `tail -n +4 ../untuned_configuration | head -n 1 | grep -oP "\\d+"`
    echo "|"
done