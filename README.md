# High-Performance Probabilistic Record Linkage via Multi-Dimensional Homomorphisms
This artifact contains the workflow to reproduce the results shown in the paper *High-Performance Probabilistic Record Linkage via Multi-Dimensional Homomorphisms*. The reviewer is invited to perform the steps as described below. In case of **any problems**, please feel free to **open an issue** in order to get in contact with the authors.

## Hardware Requirements
- At least one OpenCL-enabled device
- At least 16GB of RAM

## Software Requirements
- A Linux operating system
- CMake 2.8.0 or higher
- A C++ Compiler supporting C++14

## Experiment Workflow
The experiment workflow is divided into three steps, that are described in detail in the following sections.

### Installation
1. Clone the repository:
```
git clone https://gitlab.com/anonymous_author/record-linkage-sac-2018.git
```
2. Change into the repository folder:
```
cd record-linkage-sac-2018
```
3. Execute the installation script:
```
./install.sh
```
### Providing the tuned and untuned parameter configurations
Before our record linkage implementation can be evaluated, it has to be auto-tuned for the desired OpenCL device:
1. Select the desired OpenCL platform and device. For example, to auto-tune for the first device of the first platform, execute:
```
export PLATFORM_ID=0
export DEVICE_ID=0
```
A list of all available OpenCL platforms and devices can be queried using the `clinfo` command.

2. Execute the tuning script:
```
./tune.sh
```
3. After the tuning script has finished, the best found parameter configuration can be found in the `tuned_configuration` textfile in the repository root folder. The untuned parameter configuration can be found in the `untuned_configuration` textfile and can be edited by hand.

### Evaluation
Execute the evaluation script:
```
./run.sh
```
This will execute (1) the Java implementation used in the EKR, (2) the tuned md_hom implementation, and (3) the untuned md_hom implementation. We show results for increasing input sizes of new records: 2^15, ..., 2^20.