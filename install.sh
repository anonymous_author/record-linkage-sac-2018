#!/bin/bash

cd "$(dirname "$0")" || exit 1
rm -rf build
mkdir -p build/data &&\
cd build &&\
cmake .. &&\
make -j `nproc --all` &&\
cp ../java/* . &&\
echo "Unzipping data files..." &&\
unzip ../data/data.zip -d data/ &&\
echo "...done"
echo &&\
echo &&\
echo "Artifact installation successful!" &&\
echo